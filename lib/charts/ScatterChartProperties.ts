export class ScatterChartProperties {
  private _minValue: number;
  private _maxValue: number;

  constructor($minValue: number, $maxValue: number) {
    this._minValue = $minValue ? $minValue : 0;
    this._maxValue = $maxValue ? $maxValue : 0;
  }

  public get minValue(): number {
    return this._minValue;
  }

  public set minValue(value: number) {
    this._minValue = value;
  }

  public get maxValue(): number {
    return this._maxValue;
  }

  public set maxValue(value: number) {
    this._maxValue = value;
  }
}
