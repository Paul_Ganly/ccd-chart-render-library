import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';

export abstract class Chart {
  private _idx: string;
  private _chartType: enums.ChartType;
  private _width: number;
  private _height: number;
  private _workspaceSize: number[];
  private _titleHeight: number;
  private _titleStyling: models.TitleStyling;
  private _legendSize: models.LegendSize;
  private _legendPosition: string;
  private _legendStyling: models.LegendStyling;
  private _legendLabels: string[];
  private _seriesConfig: models.SeriesConfig;
  private _axisConfig: models.AxisConfig;
  private _xAxisSpacing: number;
  private _yAxisSpacing: number;
  private _color: any;
  private _margin: models.Margin;
  private _colors: string[];
  private _donutChartProperties: charts.DonutChartProperties;
  private _lineChartProperties: charts.LineChartProperties;
  private _pieChartProperties: charts.PieChartProperties;
  private _scatterChartProperties: charts.ScatterChartProperties;

  constructor($chartProperties: charts.IChartProperties) {
    this._idx = $chartProperties.idx;
    this._chartType = $chartProperties.chartType;
    this._width = $chartProperties.width;
    this._height = $chartProperties.height;
    this._workspaceSize =  $chartProperties.workspaceSize;
    this._titleHeight = $chartProperties.titleHeight;
    this._titleStyling =  $chartProperties.titleStyling;
    this._legendSize =  $chartProperties.legendSize;
    this._legendPosition =  $chartProperties.legendPosition;
    this._legendStyling =  $chartProperties.legendStyling;
    this._legendLabels =  $chartProperties.legendLabels;
    this._seriesConfig =  $chartProperties.seriesConfig;
    this._axisConfig =  $chartProperties.axisConfig;
    this._xAxisSpacing =  $chartProperties.xAxisSpacing;
    this._yAxisSpacing = $chartProperties.yAxisSpacing;
    this._color = $chartProperties.color;
    this._margin = $chartProperties.margin;
    this._colors = $chartProperties.colors;
    this._donutChartProperties = $chartProperties.donutChartProperties;
    this._lineChartProperties = $chartProperties.lineChartProperties;
    this._pieChartProperties = $chartProperties.pieChartProperties;
    this._scatterChartProperties = $chartProperties.scatterChartProperties;
  }

  abstract renderObject(d3Lib: any): any;

  public get idx(): string {
    return this._idx;
  }

  public set idx(value: string) {
    this._idx = value;
  }

  public get chartType(): enums.ChartType {
    return this._chartType;
  }

  public set chartType(value: enums.ChartType) {
    this._chartType = value;
  }

  public get width(): number {
    return this._width;
  }

  public set width(value: number) {
    this._width = value;
  }

  public get height(): number {
    return this._height;
  }

  public set height(value: number) {
    this._height = value;
  }

  public get workspaceSize(): number[] {
    return this._workspaceSize;
  }

  public set workspaceSize(value: number[]) {
    this._workspaceSize = value;
  }

  public get titleHeight(): number {
    return this._titleHeight;
  }

  public set titleHeight(value: number) {
    this._titleHeight = value;
  }

  public get titleStyling(): models.TitleStyling {
    return this._titleStyling;
  }

  public set titleStyling(value: models.TitleStyling) {
    this._titleStyling = value;
  }

  public get colors(): string[] {
    return this._colors;
  }

  public set colors(value: string[]) {
    this._colors = value;
  }

  public get legendSize(): models.LegendSize {
    return this._legendSize;
  }

  public set legendSize(value: models.LegendSize) {
    this._legendSize = value;
  }

  public get legendPosition(): string {
    return this._legendPosition;
  }

  public set legendPosition(value: string) {
    this._legendPosition = value;
  }

  public get legendStyling(): models.LegendStyling {
    return this._legendStyling;
  }

  public set legendStyling(value: models.LegendStyling) {
    this._legendStyling = value;
  }

  public get legendLabels(): string[] {
    return this._legendLabels;
  }

  public set legendLabels(value: string[]) {
    this._legendLabels = value;
  }

  public get seriesConfig(): models.SeriesConfig {
    return this._seriesConfig;
  }

  public set seriesConfig(value: models.SeriesConfig) {
    this._seriesConfig = value;
  }

  public get axisConfig(): models.AxisConfig {
    return this._axisConfig;
  }

  public set axisConfig(value: models.AxisConfig) {
    this._axisConfig = value;
  }

  public get xAxisSpacing(): number {
    return this._xAxisSpacing;
  }

  public set xAxisSpacing(value: number) {
    this._xAxisSpacing = value;
  }

  public get yAxisSpacing(): number {
    return this._yAxisSpacing;
  }

  public set yAxisSpacing(value: number) {
    this._yAxisSpacing = value;
  }

  public get color(): any {
    return this._color;
  }

  public set color(value: any) {
    this._color = value;
  }

  public get margin(): models.Margin {
    return this._margin;
  }

  public set margin(value: models.Margin) {
    this._margin = value;
  }
 
	public get donutChartProperties(): charts.DonutChartProperties {
		return this._donutChartProperties;
	}

	public set donutChartProperties(value: charts.DonutChartProperties) {
		this._donutChartProperties = value;
  }
  
  public get lineChartProperties(): charts.LineChartProperties {
		return this._lineChartProperties;
	}

	public set lineChartProperties(value: charts.LineChartProperties) {
		this._lineChartProperties = value;
	}

	public get pieChartProperties(): charts.PieChartProperties {
		return this._pieChartProperties;
	}

	public set pieChartProperties(value: charts.PieChartProperties) {
		this._pieChartProperties = value;
  }

  public get scatterChartProperties(): charts.ScatterChartProperties {
		return this._scatterChartProperties;
	}
  
  public set scatterChartProperties(value: charts.ScatterChartProperties) {
		this._scatterChartProperties = value;
  }
}
