export class LineChartProperties {
  private _isCurved: boolean;
  private _lastXAxisWidth: number;

  constructor($isCurved: boolean, $lastXAxisWidth: number) {
    this._isCurved = $isCurved ? $isCurved : false;
    this._lastXAxisWidth = $lastXAxisWidth ? $lastXAxisWidth : 0;
  }

  public get isCurved(): boolean {
    return this._isCurved;
  }

  public set isCurved(value: boolean) {
    this._isCurved = value;
  }

  public get lastXAxisWidth(): number {
    return this._lastXAxisWidth;
  }

  public set lastXAxisWidth(value: number) {
    this._lastXAxisWidth = value;
  }
}
