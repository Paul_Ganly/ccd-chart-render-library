export class PieChartProperties {
  private _spaceForLabels: number;

  constructor($spaceForLabels: number) {
    this._spaceForLabels = $spaceForLabels ? $spaceForLabels : 0;
  }

  public get spaceForLabels(): number {
    return this._spaceForLabels;
  }

  public set spaceForLabels(value: number) {
    this._spaceForLabels = value;
  }
}
