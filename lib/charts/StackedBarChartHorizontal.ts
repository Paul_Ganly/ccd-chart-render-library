import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class StackedBarChartHorizontal extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
      super(chart);
      this.data = data;
      this.isHeadlessRender = isHeadlessRender;
    }
  
    public renderObject(d3Lib: any): any {
      let d3: any;
      if (this.isHeadlessRender) {
        d3 = d3Lib.d3;
      } else {
        d3 = d3Lib;
      }
  
      if (this.colors.length === 2) {
        this.color = d3
          .scaleLinear()
          .domain([0, this.data.length - 1])
          .interpolate(d3.interpolateRgb)
          .range(this.colors);
      } else {
        this.color = d3.scaleOrdinal().range(this.colors);
      }
  
      // spacing of title on axis x
      let fontTitleSpacingX: number = 0;
      if (this.axisConfig.horizontal.title.titleText.length) {
          fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
      }

      // spacing of title on axis y
      let fontTitleSpacingY: number = 0;
      if (this.axisConfig.vertical.title.titleText.length) {
          fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
      }

      // Total values
      const totalValues = [];
      let sum;
      for (let i = 0; i < this.data.series[0].values.length; i++) {
          sum = 0;
          for (let j = 0; j < this.data.series.length; j++) {
              sum += this.data.series[j].values[i];
          }

          totalValues.push(sum);
      }

      // Normalized array
      const normalizedData: any[] = [];
      for (let i = 0; i < this.data.series[0].values.length; i++) {
          for (let j = 0; j < this.data.series.length; j++) {
              normalizedData.push(this.data.series[j].values[i]);
          }
      }

      // init svg
      let svg;
      if (this.isHeadlessRender) {
          svg = d3.select(d3Lib.document.querySelector('svg'));
      } else {
          svg = d3.select(`svg#${this.idx}`);
      }

      svg.append('g')
          .attr('class', 'x-group')
          .selectAll('.labels-x')
          .data(normalizedData)
          .enter()
          .append('text')
          .text((d: any) => d)
          .style('font-size', this.axisConfig.horizontal.axis.size.value)
          .style('transform', () => {
              if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                  return `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg)`;
              } else {
                  return 'rotate(0deg)';
              }
          })
          .each(() => {
            if (this.isHeadlessRender) {
              if (d3.select(d3Lib.document.querySelector('.x-group')).node()){
                  this.xAxisSpacing = d3.select(d3Lib.document.querySelector('.x-group')).node().getBBox().height;
              }
            } else {
                if (d3.select('.x-group').node()) {
                    this.xAxisSpacing = d3.select('.x-group').node().getBBox().height;
                }
            }
          });

      if (this.isHeadlessRender) {
          d3.select(d3Lib.document.querySelector('.x-group')).remove();
      } else {
          d3.select('.x-group').remove();
      }

      svg.append('g')
          .attr('class', 'y-group')
          .selectAll('.labels-y')
          .data(this.data.labels)
          .enter()
          .append('text')
          .text((d: any) => d)
          .style('font-size', this.axisConfig.vertical.axis.size.value)
          .each(() => {
            if (this.isHeadlessRender) {
              if (d3.select(d3Lib.document.querySelector('.y-group')).node()) {
                  this.yAxisSpacing = d3.select(d3Lib.document.querySelector('.y-group')).node().getBBox().width;
              }
            } else {
              if (d3.select('.y-group').node()) {
                  this.yAxisSpacing = d3.select('.y-group').node().getBBox().width;
              }
            }
          });

        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.y-group')).remove();
        } else {
            d3.select('.y-group').remove();
        }

      const empty: any = '';
      const width = this.width - this.yAxisSpacing - fontTitleSpacingY - this.margin.left - this.margin.right;
      const height = this.height - this.xAxisSpacing - fontTitleSpacingX - this.margin.top - this.margin.bottom;
      const gapBetweenGroups = (this.height / this.data.labels.length) / 5;
      const barHeight = ((height - (gapBetweenGroups * this.data.labels.length)) / this.data.labels.length);

      // Labels array
      const aLabels = _.map(this.data.series, (element, index) => {
          return this.data.series[index].label;
      });

      // Calculate height based on data length
      const chartHeight = (barHeight + gapBetweenGroups) * this.data.labels.length;

      // Set group for axis labels
      const g = svg.append('g')
          .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

      // init axis
      const x = d3.scaleLinear().range([0, width]);
      const y = d3.scaleBand().range([chartHeight, 0]);
      x.domain([0, d3.max(totalValues)]);
      y.domain(_.map(this.data.labels, (d: any) => d));

      // draw axis
      if (this.axisConfig.horizontal.axis.display) {
          // Axis configuration
          const xAxis = d3.axisBottom(x)
              .ticks(10)
              .tickFormat((d: any): any => {
                  return parseInt(d, 10);
              }).tickSizeInner(chartHeight * -1);

          // Draw x axis
          g.append('g')
              .attr('class', 'axis x-axis')
              .attr('transform', `translate(${this.yAxisSpacing + fontTitleSpacingY}, ${height})`)
              .call(xAxis);

          let xAxisLabel;
          if (this.isHeadlessRender) {
              xAxisLabel = d3.select(d3Lib.document.querySelector('.x-axis text'));
          } else {
              xAxisLabel = d3.select('.x-axis text');
          }
          
          // label rotation
          if (this.axisConfig.horizontal.axis.rotation.value > 0) {
              xAxisLabel
                  .style('text-anchor', 'end')
                  .style('transform', `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg) translate(${-5 - (this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px, ${-(this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px)`);
          }

          // label styling
          xAxisLabel
              .attr('class', (d: any, i: any) => {
                  return `axis-label-${i}`;
              })
              .attr('fill', this.axisConfig.horizontal.axis.color.color)
              .style('font-size', this.axisConfig.horizontal.axis.size.value)
              .style('font-weight', () => {
                  return this.axisConfig.horizontal.axis.isBold ? 'bold' : 'normal';
              })
              .style('font-style', () => {
                  return this.axisConfig.horizontal.axis.isItalic ? 'italic' : 'normal';
              });
      }

      if (this.axisConfig.vertical.axis.display) {
          // draw Y axis
          const yAxis = g.append('g')
              .attr('class', 'axis y-axis')
              .attr('transform', `translate(${this.yAxisSpacing + fontTitleSpacingY}, 0)`)
              .call(d3.axisLeft(y));

          if (this.isHeadlessRender) {
              d3.select(d3Lib.document.querySelector('.y-axis .domain')).style('display', 'none');
          } else {
              d3.select('.y-axis .domain').style('display', 'none');
          }

          let yAxisLabel;
          if (this.isHeadlessRender) {
              yAxisLabel = d3.select(d3Lib.document.querySelector('.y-axis text'));
          } else {
              yAxisLabel = d3.select('.y-axis text');
          }

          // label styling
          yAxisLabel.attr('class', (d: any, i: any) => {
                  return `axis-label-${i}`;
              })
              .attr('fill', this.axisConfig.vertical.axis.color.color)
              .style('font-size', this.axisConfig.vertical.axis.size.value)
              .style('font-weight', () => {
                  return this.axisConfig.vertical.axis.isBold ? 'bold' : 'normal';
              })
              .style('font-style', () => {
                  return this.axisConfig.vertical.axis.isItalic ? 'italic' : 'normal';
              });
      }

      // Set group for all bars
      const barsGroup = svg.append('g')
          .attr('transform', 'translate(' + (this.margin.left + this.yAxisSpacing + fontTitleSpacingY) + ',' + this.margin.top
          + ')');

      // Create bars
      const bar = barsGroup.selectAll('.groups')
          .data(normalizedData)
          .enter()
          .append('g')
          .attr('class', 'groups')
          .attr('transform', (d: any, i: any) => {
              return `translate(0, ${(((Math.floor(i / this.data.series.length)) * barHeight) + gapBetweenGroups * (0.5 + Math.floor(i / this.data.series.length)))})`;
          });

      // Create rectangles of the correct width
      let barPositionX = 0;

      bar.append('rect')
          .attr('fill', (d: any, i: any) => {
              return this.color(i % this.data.series.length);
          })
          .attr('class', 'bar')
          .attr('width', x)
          .attr('height', barHeight)
          .attr('x', (d: any, i: any) => {
              if ((i % this.data.series.length) === 0) {
                  barPositionX = 0;
                  return barPositionX;
              }
              for (let j = i; j < normalizedData.length; j++) {
                  if ((i - j) % this.data.series.length === 0) {
                      barPositionX += x(normalizedData[i - 1]);
                      return barPositionX;
                  }
              }
          });

      // Draw labels
      if (this.seriesConfig.display) {
          // Draw labels
          let textPositionX = 0;

          bar.append('text')
          .attr('text-anchor', 'middle')
          .attr('alignment-baseline', 'middle')
          .attr('x', (d: any, i: any) => {
              if ((i % this.data.series.length) === 0) {
                  textPositionX = x(normalizedData[i]);
                  return textPositionX / 2;
              }
              for (let j = i; j < normalizedData.length; j++) {
                  if ((i - j) % this.data.series.length === 0) {
                      textPositionX += x(normalizedData[j]);
                      return textPositionX - (x(normalizedData[j]) / 2);
                  }
              }

          })
          .attr('y', (d: any, i: any) => {
              return barHeight / 2;
          })
          .attr('fill', this.seriesConfig.color)
          .style('font-size', this.seriesConfig.size)
          .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
          .text((d: any) => d);
      }

      // x axis text
      if (this.axisConfig.horizontal.title.titleText.length > 0) {
          let xAxisText;
          if (this.isHeadlessRender) {
              xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
          } else {
              xAxisText = d3.select('.x-axis');
          }
          
          xAxisText
              .append('text')
              .attr('class', 'x-title')
              .attr('text-anchor', 'middle')
              .attr('alignment-baseline', 'hanging')
              .text(this.axisConfig.horizontal.title.titleText)
              .attr('fill', this.axisConfig.horizontal.title.color.color)
              .style('font-size', this.axisConfig.horizontal.title.size.value)
              .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
              .style('font-weight', () => {
                  return this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
              })
              .style('font-style', () => {
                  return this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
              })
              .attr('transform', `translate(${width / 2}, 0)`)
              .attr('y', this.xAxisSpacing + 20);
      }

      // y axis text
      if (this.axisConfig.vertical.title.titleText.length > 0) {
          let yAxisText;
          if (this.isHeadlessRender) {
          yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
          } else {
          yAxisText = d3.select('.y-axis');
          }

          yAxisText
              .append('text')
              .attr('class', 'y-title')
              .attr('text-anchor', 'middle')
              .attr('alignment-baseline', 'hanging')
              .attr('x', (d: any) => {
                  return -(height / 2);
              })
              // horizontal distance
              .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
              .text(this.axisConfig.vertical.title.titleText)
              .attr('fill', this.axisConfig.vertical.title.color.color)
              .style('font-size', this.axisConfig.vertical.title.size.value)
              .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
              .style('font-weight', () => {
                  return this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
              })
              .style('font-style', () => {
                  return this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
              })
              .style('transform', 'rotate(-90deg)');
      }
      return svg;
    }
  }