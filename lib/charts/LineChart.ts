import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class LineChart extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
        super(chart);
        this.data = data;
        this.isHeadlessRender = isHeadlessRender;
    } 
 
    public renderObject(d3Lib: any): any {
        let d3: any;
        if (this.isHeadlessRender) {
          d3 = d3Lib.d3;
        } else {
          d3 = d3Lib;
        }

        // spacing of title on axis x
        let fontTitleSpacingX: number = 0;
        if (this.axisConfig.horizontal.title.titleText.length) {
            fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
        }

        // spacing of title on axis y
        let fontTitleSpacingY: number = 0;
        if (this.axisConfig.vertical.title.titleText.length) {
            fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
        }

        // spacing of value labels
        let valuesLabelSpacing: number = 0;
        if (this.seriesConfig.display) {
            valuesLabelSpacing = this.seriesConfig.size;
        }

        const normalizedData = _.map(this.data.labels, (item, index) => {
            return {
                labels: this.data.labels[index],
                values: _.map(this.data.series, (elem: any) => {
                    return elem.values[index];
                })
            };
        });

        // init svg
        let svg;
        if (this.isHeadlessRender) {
          svg = d3.select(d3Lib.document.querySelector('svg'));
        } else {
          svg = d3.select(`svg#${this.idx}`);
        }
    
        svg.attr('height', this.height)
            .attr('width', this.width);

        const XaxisLabels = _.map(this.data.series, (series: any) => series.label);
        // Normalize array
        const valuesArray = [];
        for (let i = 0; i < this.data.labels.length; i++) {
            for (let j = 0; j < this.data.series.length; j++) {
                valuesArray.push(this.data.series[j].values[i]);
            }
        }

        svg.append('g')
            .attr('class', 'x-group')
            .selectAll('.labels-x')
            .data(XaxisLabels)
            .enter()
            .append('text')
            .text((d: any) => d)
            .style('font-size', this.axisConfig.horizontal.axis.size.value)
            .style('transform', () => {
                if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                    return `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg)`;
                } else {
                    return 'rotate(0deg)';
                }
            })
            .each(() => {
                this.xAxisSpacing = d3.select('.x-group' as any).node().getBBox().height;
            });

        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.x-group')).remove();
        } else {
            d3.select('.x-group').remove();
        }

        svg.append('g')
            .attr('class', 'y-group')
            .selectAll('.labels-y')
            .data(valuesArray)
            .enter()
            .append('text')
            .text((d: any) => d)
            .style('font-size', this.axisConfig.vertical.axis.size.value)
            .each(() => {
                this.yAxisSpacing = d3.select('.y-group' as any).node().getBBox().width;
            });

        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.y-group')).remove();
        } else {
            d3.select('.y-group').remove();
        }

        svg.append('g')
            .attr('class', 'last-x-label')
            .selectAll('.last-axis-label')
            .data([XaxisLabels.pop()])
            .enter()
            .append('text')
            .text((d: any) => d)
            .style('font-size', this.axisConfig.horizontal.axis.size.value)
            .style('transform', () => {
                if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                    return `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg)`;
                } else {
                    return 'rotate(0deg)';
                }
            })
            .each(() => {
                this.lineChartProperties.lastXAxisWidth = d3.select('.last-x-label' as any).node().getBBox().width;
            });

        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.last-x-label')).remove();
        } else {
            d3.select('.last-x-label').remove();
        }

        const width = this.width - this.yAxisSpacing - fontTitleSpacingY - this.margin.left;
        const height = this.height - this.xAxisSpacing - fontTitleSpacingX - this.margin.top - this.margin.bottom - valuesLabelSpacing;

        const g = svg.append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top + valuesLabelSpacing})`)
            .attr('class', 'group');

        // init axis
        const x = d3.scaleLinear().rangeRound([this.yAxisSpacing + fontTitleSpacingY, width + this.yAxisSpacing  + fontTitleSpacingY - (this.lineChartProperties.lastXAxisWidth / 2) - (valuesLabelSpacing / 2)]);
        const y = d3.scaleLinear().rangeRound([height, 0]);
        x.domain([0, d3.max(normalizedData, (d: any) => {
            return d.values.length - 1;
        })]);
        y.domain([0, d3.max(valuesArray)]);

        // draw axis
        if (this.axisConfig.horizontal.axis.display) {
            // Axis configuration
            const xAxis = d3.axisBottom(x)
                .ticks(this.data.series.length - 1)
                .tickFormat((d: any, i: any) => {
                    return this.data.series[i].label;
                });

            // Draw x axis
            g.append('g')
                .attr('class', 'axis x-axis')
                .attr('transform', 'translate(0,' + height + ')')
                .call(xAxis);

            // d3.selectAll('.x-axis line').style('display', 'none');

            // label styling
            let xAxisLabels;
            if (this.isHeadlessRender) {
                xAxisLabels = d3.select(d3Lib.document.querySelector('.x-axis text'));
            } else {
                xAxisLabels = d3.select('.x-axis text');
            }

            xAxisLabels.attr('class', (d: any, i: any) => {
                    return `axis-label-${i}`;
                })
                .attr('fill', this.axisConfig.horizontal.axis.color.color)
                .style('font-size', this.axisConfig.horizontal.axis.size.value)
                .style('font-weight', () => {
                    return this.axisConfig.horizontal.axis.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.horizontal.axis.isItalic ? 'italic' : 'normal';
                });

            if (this.axisConfig.horizontal.axis.display) {
                if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                    xAxisLabels.style('text-anchor', 'end')
                        .style('transform', `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg) translate(${-5 - (this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px, ${-(this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px)`);
                }
            }
        }

        if (this.axisConfig.vertical.axis.display) {
            const yAxis = d3.axisLeft(y)
                .tickFormat(d3.format('d'));

            // Draw y axis
            g.append('g')
                .attr('class', 'axis y-axis')
                .attr('transform', `translate(${this.yAxisSpacing + fontTitleSpacingY}, 0)`)
                .call(yAxis);

            // label styling
            let yAxisLabels;
            if (this.isHeadlessRender) {
                yAxisLabels = d3.select(d3Lib.document.querySelector('.y-axis text'));
            } else {
                yAxisLabels = d3.select('.y-axis text');
            }
            yAxisLabels.attr('class', (d: any, i: any) => {
                    return `axis-label-${i}`;
                })
                .attr('fill', this.axisConfig.vertical.axis.color.color)
                .style('font-size', this.axisConfig.vertical.axis.size.value)
                .style('font-weight', () => {
                    return this.axisConfig.vertical.axis.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.vertical.axis.isItalic ? 'italic' : 'normal';
                });
        }

        let lineHandler = d3.line()
                .x(function(d: any, i: any): any { return x(i); })
                .y(function(d: any, i: any): any { return y(d); });

        if (this.lineChartProperties.isCurved) {
            lineHandler = d3.line()
                .curve(d3.curveMonotoneX)
                .x(function(d: any, i: any): any { return x(i); })
                .y(function(d: any, i: any): any { return y(d); });
        }

        // angular 'this' issue
        const color = this.color;

        const lines = g.selectAll('.lines')
            .data(normalizedData)
            .enter()
            .append('g')
            .attr('class', 'lines');

        lines.append('path')
            .attr('class', 'line')
            .style('fill', 'none')
            .attr('d', function(d: any): any { return lineHandler(d.values); })
            .style('stroke', (d: any, i: any) => color(d.labels));

        lines.append('g')
            .attr('title', function(d: any): any {
                return d.labels;
            })
            .selectAll('circle')
            .data(function(d: any): any {
                return d.values;
            })
            .enter()
            .append('circle')
            .attr('r', 3)
            .attr('cx', function(d: any, i: any): any { return x(i); })
            .attr('cy', function(d: any, i: any): any { return y(d); })
            .attr('fill', 'white')
            .attr('stroke', function(d: any, i: any): any {
                return color(d);
            });

        if (this.seriesConfig.display) {
            // labels
            lines
                .selectAll('text')
                .data(function(d: any): any {
                    return d.values;
                })
                .enter()
                .append('text')
                .attr('text-anchor', 'middle')
                .attr('x', function(d: any, i: any): any { return x(i); })
                .attr('y', function(d: any, i: any): any { return y(d) - 10; })
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .text(function(d: any, i: any): any {
                    return d;
                });
        }

        // x axis text
        if (this.axisConfig.horizontal.title.titleText.length > 0) {
            let xAxisText;
            if (this.isHeadlessRender) {
              xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
            } else {
              xAxisText = d3.select('.x-axis');
            }

            xAxisText.append('text')
                .attr('class', 'x-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .text(this.axisConfig.horizontal.title.titleText)
                .attr('fill', this.axisConfig.horizontal.title.color.color)
                .style('font-size', this.axisConfig.horizontal.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', () => {
                    return this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
                })
                .attr('x', (d: any) => {
                    return this.width / 2;
                })
                .attr('y', this.xAxisSpacing + 20);
        }

        // y axis text
        if (this.axisConfig.vertical.title.titleText.length > 0) {
            let yAxisText;
            if (this.isHeadlessRender) {
              yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
            } else {
              yAxisText = d3.select('.y-axis');
            }

            yAxisText.append('text')
                .attr('class', 'y-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .attr('x', (d: any) => {
                    return -(height / 2);
                })
                // horizontal distance
                .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
                .text(this.axisConfig.vertical.title.titleText)
                .attr('fill', this.axisConfig.vertical.title.color.color)
                .style('font-size', this.axisConfig.vertical.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', () => {
                    return this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
                })
                .style('transform', 'rotate(-90deg)');
        }
        return svg;

    }
}