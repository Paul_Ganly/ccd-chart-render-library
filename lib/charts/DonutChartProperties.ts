export class DonutChartProperties {
  private _shadows: boolean;
  private _spaceForLabels: number;

  constructor($shadows: boolean, $spaceForLabels: number) {
    this._shadows = $shadows ? $shadows : false;
    this._spaceForLabels = $spaceForLabels ? $spaceForLabels : 0;
  }

  public get shadows(): boolean {
    return this._shadows;
  }

  public set shadows(value: boolean) {
    this._shadows = value;
  }

  public get spaceForLabels(): number {
    return this._spaceForLabels;
  }

  public set spaceForLabels(value: number) {
    this._spaceForLabels = value;
  }
}
