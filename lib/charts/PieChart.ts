import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class PieChart extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
        super(chart);
        this.data = data;
        this.isHeadlessRender = isHeadlessRender;
    } 
 
    public renderObject(d3Lib: any): any {
        let d3: any;
        if (this.isHeadlessRender) {
          d3 = d3Lib.d3;
        } else {
          d3 = d3Lib;
        }

        const radius = Math.min(this.width, this.height) / 2;

        const outerRadius = radius - this.pieChartProperties.spaceForLabels;

        // data totals
        const dataSum = d3.sum(this.data, (d: any) => d.value);

        const chartData = _.map(this.data, (element, index) => {
            return this.data[index].value;
        });

        // section arc
        const sectionArc: any = d3.arc()
            .innerRadius(0)
            .outerRadius(outerRadius);

        // value calc
        const pieHandler = d3.pie()
            .sort(undefined)
            .value((d: any): any => {
                return d;
            });

        // init svg
        let svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        } else {
            svg = d3.select(`svg#${this.idx}`);
        }

        svg.attr('height', this.height)
            .attr('width', this.width)
            .append('g')
            .attr('transform', `translate(${this.width / 2},${radius})`)
            .attr('class', 'group');

        // sections group
        const g = svg.selectAll('.arc')
            .data(pieHandler(chartData))
            .enter().append('g')
            .attr('class', 'arc');

        // sections
        g.append('path')
            .attr('d', sectionArc)
            .attr('class', 'pie-slide')
            .attr('fill', (d: any, i: any) => {
                return this.color(i);
            });

        if (this.seriesConfig.display) {
            // sections label
            svg.append('g')
                .attr('class', 'labels')
                .selectAll('text')
                .data(pieHandler(chartData))
                .enter()
                .append('text')
                .attr('transform', (d: any) => {
                    return `translate(${sectionArc.centroid(d)})`;
                })
                .attr('dy', '.35em')
                .attr('text-anchor', (d: any) => {
                    return (d.endAngle + d.startAngle) / 2 > Math.PI ? 'end' : 'start';
                })
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .attr('transform', (d: any) => {
                    const labelr = outerRadius + 10;
                    const c = sectionArc.centroid(d);
                    const x = c[0];
                    const y = c[1];
                    // pythagorean theorem for hypotenuse
                    const h = Math.sqrt((x * x) + (y * y));
                    return `translate(${((x / h) * labelr)},${((y / h) * labelr)})`;
                })
                .text((d: any) => {
                    const value = (d.value / dataSum) * 100;
                    return `${d3.format('.3g')(value)}%`;
                });
        }

        return svg;

    }
}