import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class DonutChart extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
      super(chart);
      this.data = data;
      this.isHeadlessRender = isHeadlessRender;
    } 
 
    public renderObject(d3Lib: any): any {
        let d3: any;
        if (this.isHeadlessRender) {
          d3 = d3Lib.d3;
        } else {
          d3 = d3Lib;
        }

        const radius = Math.min(this.width, this.height) / 2;

        const outerRadius = radius - this.donutChartProperties.spaceForLabels;
        const innerRadius = Math.round(outerRadius / 2);

        // data totals
        const dataSum = d3.sum(this.data, (d: any) => d.value);

        const chartData = _.map(this.data, (item: any) => item.value);

        // section arc
        const sectionArc: any = d3.arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius)
            .padAngle(2 / 100)
            .cornerRadius(2);

        // shadow arc
        const shadowArc: any = d3.arc()
            .innerRadius(innerRadius - 5)
            .outerRadius(innerRadius + 10)
            .padAngle(2 / 100);

        // value calc
        const pieHandler = d3.pie()
            .sort(undefined)
            .value((d: any): number => {
                return d;
            });

        // init svg
        let svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        } else {
            svg = d3.select(`svg#${this.idx}`);
        }
        
        svg.attr('height', this.height)
            .attr('width', this.width)
            .append('g')
            .attr('transform', `translate(${this.width / 2},${radius})`)
            .attr('class', 'group');

        // sections group
        const g = svg.selectAll('.arc')
            .data(pieHandler(chartData))
            .enter()
            .append('g')
            .attr('class', 'arc');

        // sections
        g.append('path')
            .attr('d', sectionArc)
            .attr('class', 'donut-slide')
            .attr('fill', (d: any, i: any) => {
                return this.color(i);
            });

        if (this.donutChartProperties.shadows) {
            // section shadows group
            const shadow = svg.selectAll('.shadows')
                .data(pieHandler(chartData))
                .enter()
                .append('g')
                .attr('class', 'shadows');

            // section shadows
            shadow.append('path')
                .attr('d', shadowArc)
                .attr('fill', (d: any, i: any): any => {
                    const c = d3.hsl(this.color(i));
                    return d3.hsl((c.h + 5), (c.s - 0.07), (c.l - 0.15));
                });
        }

        if (this.seriesConfig.display) {
            // sections label
            svg.append('g')
                .attr('class', 'labels')
                .selectAll('text')
                .data(pieHandler(chartData))
                .enter()
                .append('text')
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .attr('transform', (d: any) => {
                    return `translate(${sectionArc.centroid(d)})`;
                })
                .attr('dy', '.35em')
                .attr('text-anchor', (d: any) => {
                    return (d.endAngle + d.startAngle) / 2 > Math.PI ? 'end' : 'start';
                })
                .attr('transform', (d: any) => {
                    const labelr = outerRadius + 10;
                    const c = sectionArc.centroid(d);
                    const x = c[0];
                    const y = c[1];
                    // pythagorean theorem for hypotenuse
                    const h = Math.sqrt((x * x) + (y * y));
                    return `translate(${((x / h) * labelr)},${((y / h) * labelr)})`;
                })
                .text((d: any) => {
                    const value = (d.value / dataSum) * 100;
                    return `${d3.format('.3g')(value)}%`;
                });
        }

        return svg;

    }
}