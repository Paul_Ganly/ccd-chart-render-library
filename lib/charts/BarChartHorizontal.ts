import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class BarChartHorizontal extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
      super(chart);
      this.data = data;
      this.isHeadlessRender = isHeadlessRender;
    }
  
    public renderObject(d3Lib: any): any {
      let d3: any;
      if (this.isHeadlessRender) {
        d3 = d3Lib.d3;
      } else {
        d3 = d3Lib;
      }
  
      if (this.colors.length === 2) {
        this.color = d3
          .scaleLinear()
          .domain([0, this.data.length - 1])
          .interpolate(d3.interpolateRgb)
          .range(this.colors);
      } else {
        this.color = d3.scaleOrdinal().range(this.colors);
      }
  
      // spacing of title on axis x
      let fontTitleSpacingX: number = 0;
      if (this.axisConfig.horizontal.title.titleText.length) {
        fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
      }
  
      // spacing of title on axis y
      let fontTitleSpacingY: number = 0;
      if (this.axisConfig.vertical.title.titleText.length) {
        fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
      }
  
      // spacing of value labels
      let valuesLabelSpacing: number = 0;
      if (this.seriesConfig.display) {
        valuesLabelSpacing = this.seriesConfig.size;
      }
      // init svg
      let svg;
      if (this.isHeadlessRender) {
        svg = d3.select(d3Lib.document.querySelector('svg'));
      } else {
        svg = d3.select(`svg#${this.idx}`);
      }
  
      svg
        .append('g')
        .attr('class', 'x-group')
        .selectAll('.labels-x')
        .data(this.data)
        .enter()
        .append('text')
        .text((d: any) => d.value)
        .style('font-size', this.axisConfig.horizontal.axis.size.value)
        .style('transform', () => {
          if (this.axisConfig.horizontal.axis.rotation.value > 0) {
            return `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg)`;
          } else {
            return 'rotate(0deg)';
          }
        })
        .each(() => {
          if (this.isHeadlessRender) {
            if (d3.select(d3Lib.document.querySelector('.x-group')).node()){
              this.xAxisSpacing = d3.select(d3Lib.document.querySelector('.x-group')).node().getBBox().height;
            }
          } else {
            if (d3.select('.x-group').node()) {
              this.xAxisSpacing = d3.select('.x-group').node().getBBox().height;
            }
          }
        });
  
      if (this.isHeadlessRender) {
        d3.select(d3Lib.document.querySelector('.x-group')).remove();
      } else {
        d3.select('.x-group').remove();
      }
  
      svg.append('g')
        .attr('class', 'y-group')
        .selectAll('.labels-y')
        .data(this.data)
        .enter()
        .append('text')
        .text((d: any) => d.label)
        .style('font-size', this.axisConfig.vertical.axis.size.value)
        .each(() => {
          if (this.isHeadlessRender) {
            if (d3.select(d3Lib.document.querySelector('.y-group')).node()) {
              this.yAxisSpacing = d3.select(d3Lib.document.querySelector('.y-group')).node().getBBox().width;
            }
          } else {
            if (d3.select('.y-group').node()) {
              this.yAxisSpacing = d3.select('.y-group').node().getBBox().width;
            }
          }
        });
  
      if (this.isHeadlessRender) {
        d3.select(d3Lib.document.querySelector('.y-group')).remove();
      } else {
        d3.select('.y-group').remove();
      }
  
      const width = this.width - this.yAxisSpacing - fontTitleSpacingY - this.margin.left - this.margin.right - valuesLabelSpacing; 
      const height = this.height - this.xAxisSpacing - fontTitleSpacingX - this.margin.top - this.margin.bottom;
  
      const g = svg
        .append('g')
        .attr('transform', 'translate(' + (this.margin.left + this.yAxisSpacing + fontTitleSpacingY) + ',' + this.margin.top + ')');
  
      // init axis
      const x = d3.scaleLinear().rangeRound([0, width]);
      const y = d3.scaleBand().rangeRound([height, 0]);
      x.domain([0, d3.max(this.data, (d: any) => d.value)]);
      y.domain(_.map(this.data, (d: any) => d.label)).padding(0.1);
  
      // draw axis
      if (this.axisConfig.horizontal.axis.display) {
        // x-axis
        const xAxis = g
          .append('g')
          .attr('class', 'axis x-axis')
          .attr('transform', `translate(0, ${height})`)
          .call(
            d3
              .axisBottom(x)
              .ticks(5)
              .tickFormat((d: any): string => {
                return parseInt(d, 10).toString();
              })
              .tickSizeInner(height * -1)
          );
  
        let xAxisText;
        if (this.isHeadlessRender) {
          xAxisText = d3.select(d3Lib.document.querySelector('.x-axis text'));
        } else {
          xAxisText = d3.select('.x-axis text');
        }
  
        // label rotation
        if (this.axisConfig.horizontal.axis.rotation.value > 0) {
          xAxisText
            .style('text-anchor', 'end')
            .style(
              'transform',
              `rotate(-${this.axisConfig.horizontal.axis.rotation.value}deg) translate(${-5 -
                (this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px, ${-(
                this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10}px)`);
        }
  
        // label styling
        xAxisText
          .attr('class', (d: any, i: any) => {
            return `axis-label-${i}`;
          })
          .attr('fill', this.axisConfig.horizontal.axis.color.color)
          .style('font-size', this.axisConfig.horizontal.axis.size.value)
          .style('font-weight', () => {
            return this.axisConfig.horizontal.axis.isBold ? 'bold' : 'normal';
          })
          .style('font-style', () => {
            return this.axisConfig.horizontal.axis.isItalic ? 'italic' : 'normal';
          });
      }
  
      if (this.axisConfig.vertical.axis.display) {
        // y-axis
        const yAxis = g
          .append('g')
          .attr('class', 'axis y-axis')
          .call(d3.axisLeft(y));
  
        let yAxisText;
        if (this.isHeadlessRender) {
          yAxisText = d3.select(d3Lib.document.querySelector('.y-axis text'));
        } else {
          yAxisText = d3.select('.y-axis text');
        }
  
        // label styling
        yAxisText
          .attr('class', (d: any, i: any) => {
            return `axis-label-${i}`;
          })
          .attr('fill', this.axisConfig.vertical.axis.color.color)
          .style('font-size', this.axisConfig.vertical.axis.size.value)
          .style('font-weight', () => {
            return this.axisConfig.vertical.axis.isBold ? 'bold' : 'normal';
          })
          .style('font-style', () => {
            return this.axisConfig.vertical.axis.isItalic ? 'italic' : 'normal';
          });
      }
  
      // draw bars
      g
        .selectAll('.bar')
        .data(this.data)
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr('x', 0)
        .attr('height', y.bandwidth())
        .attr('y', (d: any) => {
          return y(d.label);
        })
        .attr('width', (d: any) => {
          return x(d.value);
        })
        .attr('fill', (d: any, i: any) => {
          return this.color(i);
        });
  
      if (this.seriesConfig.display) {
        // labels
        g
          .selectAll('.text')
          .data(this.data)
          .enter()
          .append('text')
          .attr('class', 'label-text')
          .attr('alignment-baseline', 'middle')
          .attr('x', (d: any) => {
            return 5 + x(d.value);
          })
          .attr('y', (d: any) => {
            return y(d.label) + y.bandwidth() / 2;
          })
          .attr('fill', this.seriesConfig.color)
          .style('font-size', this.seriesConfig.size)
          .style(
            'font-family',
            'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif'
          )
          .text((d: any) => {
            return d.value;
          });
      }
  
      // x axis text
      if (this.axisConfig.horizontal.title.titleText.length > 0) {
        let xAxisText;
        if (this.isHeadlessRender) {
          xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
        } else {
          xAxisText = d3.select('.x-axis');
        }
  
        xAxisText
          .append('text')
          .attr('class', 'x-title')
          .attr('text-anchor', 'middle')
          .attr('alignment-baseline', 'hanging')
          .text(this.axisConfig.horizontal.title.titleText)
          .attr('fill', this.axisConfig.horizontal.title.color.color)
          .style('font-size', this.axisConfig.horizontal.title.size.value)
          .style(
            'font-family',
            'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif'
          )
          .style('font-weight', () => {
            return this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
          })
          .style('font-style', () => {
            return this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
          })
          .attr('transform', `translate(${width / 2}, 0)`)
          .attr('y', this.xAxisSpacing + 20);
      }
  
      // y axis text
      if (this.axisConfig.vertical.title.titleText.length > 0) {
        let yAxisText;
        if (this.isHeadlessRender) {
          yAxisText = d3.select(d3Lib.document.querySelector('.y-axis text'));
        } else {
          yAxisText = d3.select('.y-axis text');
        }
  
        yAxisText
          .append('text')
          .attr('class', 'y-title')
          .attr('text-anchor', 'middle')
          .attr('alignment-baseline', 'hanging')
          .attr('x', (d: any) => {
            return -(height / 2);
          })
          // horizontal distance
          .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
          .text(this.axisConfig.vertical.title.titleText)
          .attr('fill', this.axisConfig.vertical.title.color.color)
          .style('font-size', this.axisConfig.vertical.title.size.value)
          .style(
            'font-family',
            'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif'
          )
          .style('font-weight', () => {
            return this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
          })
          .style('font-style', () => {
            return this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
          })
          .style('transform', 'rotate(-90deg)');
      }
  
      return svg;
    }
  }