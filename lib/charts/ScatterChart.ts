import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class ScatterChart extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
      super(chart);
      this.data = data;
      this.isHeadlessRender = isHeadlessRender;
    } 
 
    public renderObject(d3Lib: any): any {
        let d3: any;
        if (this.isHeadlessRender) {
          d3 = d3Lib.d3;
        } else {
          d3 = d3Lib;
        }

        const width = this.width;
        const height = this.height;

        const padding = {
            top: 20,
            right: 20,
            bottom: 40,
            left: 50
        };

        // inner chart dimensions, where the dots are plotted
        const plotAreaWidth = width - padding.left - padding.right;
        const plotAreaHeight = height - padding.top - padding.bottom;

        // radius of points in the scatterplot
        const pointRadius = 3;

        const xScale = d3.scaleLinear().domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue]).range([0, plotAreaWidth]);
        const yScale = d3.scaleLinear().domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue]).range([plotAreaHeight, 0]);

        const rScale = d3.scaleLinear()
            .domain([0, this.scatterChartProperties.maxValue])
            .range([3, 3]);

        const color = d3.scaleLinear()
            .domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue] as any)
            .range(['#eb001b', '#f79e1b'] as any)
            .interpolate(d3.interpolateHcl as any);

              // init svg
        let svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        } else {
            svg = d3.select(`svg#${this.idx}`);
        }

        svg.attr('height', this.height)
            .attr('width', this.width)
            .append('g')
            .attr('transform', `translate(${padding.left}, ${padding.top})`)
            .attr('class', 'group');

        // init axis
        const xAxisG = svg.append('g')
            .classed('x-axis', true)
            .attr('transform', `translate(0 ${plotAreaHeight + pointRadius})`);

        const yAxisG = svg.append('g')
            .classed('y-axis', true)
            .attr('transform', `translate(${-pointRadius} 0)`);

        // set up axis generating functions
        const xTicks = Math.round(plotAreaWidth / 50);
        const yTicks = Math.round(plotAreaHeight / 50);

        const xAxis = d3.axisBottom(xScale)
            .ticks(xTicks)
            .tickSizeOuter(0);

        const yAxis = d3
            .axisLeft(yScale)
            .ticks(yTicks)
            .tickSizeOuter(0);

        // draw axis
        yAxisG.call(yAxis);
        xAxisG.call(xAxis);

        const circles = svg.append('g')
            .attr('class', 'circles');

        // draw points
        circles.selectAll('.data-point')
            .data(this.data, (d: any): any => {
                return d.id;
            })
            .enter()
            .append('circle')
            .classed('data-point', true)
            .attr('r', pointRadius)
            .attr('cx', (d: any) => xScale(d.x))
            .attr('cy', (d: any) => yScale(d.y))
            .attr('fill', (d: any) => color(d.y))
            .attr('r', (d: any): any => {
                return rScale(d.v);
            });

        return svg;

    }
}