import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';

export interface IChartProperties {
  idx: string;
  chartType: enums.ChartType;
  width: number;
  height: number;
  workspaceSize: number[];
  titleHeight: number;
  titleStyling: models.TitleStyling;
  legendSize: models.LegendSize;
  legendPosition: string;
  legendStyling: models.LegendStyling;
  legendLabels: string[];
  seriesConfig: models.SeriesConfig;
  axisConfig: models.AxisConfig;
  xAxisSpacing: number;
  yAxisSpacing: number;
  color: any;
  margin: models.Margin;
  colors: string[];
  donutChartProperties: charts.DonutChartProperties;
  lineChartProperties: charts.LineChartProperties;
  pieChartProperties: charts.PieChartProperties;
  scatterChartProperties: charts.ScatterChartProperties;
}
