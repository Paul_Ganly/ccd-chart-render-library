import * as enums from '../enums/enums';
import * as models from '../models/models';
import * as charts from './charts';
import * as _ from 'lodash';

export class StackedAreaChart extends charts.Chart {
    private data: any;
    private isHeadlessRender: boolean;
  
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties) {
      super(chart);
      this.data = data;
      this.isHeadlessRender = isHeadlessRender;
    }
  
    public renderObject(d3Lib: any): any {
      let d3: any;
      if (this.isHeadlessRender) {
        d3 = d3Lib.d3;
      } else {
        d3 = d3Lib;
      }
  
      if (this.colors.length === 2) {
        this.color = d3
          .scaleLinear()
          .domain([0, this.data.length - 1])
          .interpolate(d3.interpolateRgb)
          .range(this.colors);
      } else {
        this.color = d3.scaleOrdinal().range(this.colors);
      }
  
      // spacing of title on axis x
      let fontTitleSpacingX: number = 0;
        if (this.axisConfig.horizontal.title.titleText.length) {
            fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
        }

        // spacing of title on axis y
        let fontTitleSpacingY: number = 0;
        if (this.axisConfig.vertical.title.titleText.length) {
            fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
        }

        const width = this.width - this.margin.left - this.margin.right;
        const height = this.height - this.margin.top - this.margin.bottom;
        const areaColors = this.color;
        const maxPointValueHeight = this.getMaxPointValue();
        const normalizedData = this.generateDataArrayForStack();

        // init axis
        const x = d3.scaleLinear().domain([0, d3.max(normalizedData, function(d: any): any { return d.length - 1; })]).range([0, width]);
        const y = d3.scaleLinear().domain([0, maxPointValueHeight]).range([height, 0]);

        // Axis configuration
        const xAxis = d3.axisBottom(x)
            .scale(x)
            .ticks(this.data.series.length - 1)
            .tickFormat((d: any, i: any) => {
                return this.data.series[i].label;
            });

        const yAxis = d3.axisLeft(y).scale(y).tickFormat(d3.format('d'));

        const area = d3.area()
            .x(function(d: any): any { return x(d.data.dataPoint); })
            .y0(function(d: any): any { return y(d[0]); })
            .y1(function(d: any): any { return y(d[1]); });

        const stack = d3.stack();

              // init svg
      let svg;
      if (this.isHeadlessRender) {
          svg = d3.select(d3Lib.document.querySelector('svg'));
      } else {
          svg = d3.select(`svg#${this.idx}`);
      }
      svg.attr('height', this.height)
            .attr('width', this.width)
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
            .attr('class', 'group');

        const keys = this.data.labels;

        // Set domains for axes
        x.domain(d3.extent(normalizedData, function(d: any): any { return d.dataPoint; }));
        y.domain([0, maxPointValueHeight]);

        stack.keys(keys);
        stack.order(d3.stackOrderNone);
        stack.offset(d3.stackOffsetNone);

        const stackedAreas = svg.selectAll('.area')
            .data(stack(normalizedData))
            .enter().append('g')
            .attr('class', function(d: any): any { return 'area ' + d.key; });

        stackedAreas.append('path')
            .attr('class', 'area')
            .attr('d', area)
            .style('fill', function(d: any): any { return areaColors(d.key); });

        svg.append('g')
            .attr('class', 'axis x-axis')
            .attr('transform', `translate(0, ${height})`)
            .call(xAxis);

        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis);

        // x axis text
        if (this.axisConfig.horizontal.title.titleText.length > 0) {
            let xAxisText;
            if (this.isHeadlessRender) {
                xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
            } else {
                xAxisText = d3.select('.x-axis');
            }
            
            xAxisText
                .append('text')
                .attr('class', 'x-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .text(this.axisConfig.horizontal.title.titleText)
                .attr('fill', this.axisConfig.horizontal.title.color.color)
                .style('font-size', this.axisConfig.horizontal.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', () => {
                    return this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
                })
                .attr('x', (d: any) => {
                    return this.width / 2;
                })
                .attr('y', this.xAxisSpacing + 20);
        }

        // y axis text
        if (this.axisConfig.vertical.title.titleText.length > 0) {
            let yAxisText;
            if (this.isHeadlessRender) {
            yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
            } else {
            yAxisText = d3.select('.y-axis');
            }
  
            yAxisText
                .append('text')
                .attr('class', 'y-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .attr('x', (d: any) => {
                    return -(height / 2);
                })
                // horizontal distance
                .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
                .text(this.axisConfig.vertical.title.titleText)
                .attr('fill', this.axisConfig.vertical.title.color.color)
                .style('font-size', this.axisConfig.vertical.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', () => {
                    return this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
                })
                .style('font-style', () => {
                    return this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
                })
                .style('transform', 'rotate(-90deg)');
        }
      return svg;
    }

    /**
    * In order to use the d3 stack function we must arrange the data in a specific format.
    * @author paulganly
    * @date   2017-02-09
    */
    public generateDataArrayForStack(): any[] {
        const normalizedDataArray = [];
        const numberOfAreasToRender = this.data.labels.length;
        const numberDataPointsPerItem = this.data.series.length;
  
        for (let i = 0; i < numberDataPointsPerItem; i++) {
            const dataArrayRow: any = {};
            dataArrayRow['dataPoint'] = this.data.series[i].label;
            for (let j = 0; j < numberOfAreasToRender; j++) {
                dataArrayRow[this.data.labels[j]] = this.data.series[i].values[j];
            }
            normalizedDataArray.push(dataArrayRow);
        }
  
        return normalizedDataArray;
      }
  
    /**
     * We must calculate the max point height when all of the area are to be stacked on top of each-other so that
     * the y-axis is scaled correctly.
     * @author paulganly
     * @date   2017-02-09
     */
    public getMaxPointValue(): any {
    const seriesArrays = this.data.series;
    const numberOfPointValues = seriesArrays[0].values.length;
    let maxPointHeight = 0;
    for (let i = 0; i < numberOfPointValues; i++) {
        let currentPointHeight = 0;
        for (let j = 0; j < seriesArrays.length; j++) {
            currentPointHeight += seriesArrays[j].values[i];
        }
        if (currentPointHeight > maxPointHeight) {
            maxPointHeight = currentPointHeight;
        }
    }
    return maxPointHeight;
    }
  
  }