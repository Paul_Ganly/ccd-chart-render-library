export class AxisTitleStyling {
    private _titleText: string;
    private _isBold: boolean;
    private _isItalic: boolean;
    private _size: any;
    private _color: any;

    constructor($titleText: string, $isBold: boolean, $isItalic: boolean, $size: any, $color: any) {
        this._titleText = $titleText ? $titleText : '';
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
    }

    public get titleText(): string {
        return this._titleText;
    }

    public set titleText(value: string) {
        this._titleText = value;
    }

    public get isBold(): boolean {
        return this._isBold;
    }

    public set isBold(value: boolean) {
        this._isBold = value;
    }

    public get isItalic(): boolean {
        return this._isItalic;
    }

    public set isItalic(value: boolean) {
        this._isItalic = value;
    }

    public get size(): any {
        return this._size;
    }

    public set size(value: any) {
        this._size = value;
    }

    public get color(): any {
        return this._color;
    }

    public set color(value: any) {
        this._color = value;
    }
}

export class AxisStyling {
    private _display: boolean;
    private _isBold: boolean;
    private _isItalic: boolean;
    private _size: any;
    private _color: any;
    private _rotation: any;

    constructor($display: boolean, $isBold: boolean, $isItalic: boolean, $size: any, $color: any, $rotation: any) {
        this._display = $display ? $display : true;
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
        this._rotation = $rotation ? $rotation : { id: 1, value: 0 };
    }

    public get display(): boolean {
        return this._display;
    }

    public set display(value: boolean) {
        this._display = value;
    }

    public get isBold(): boolean {
        return this._isBold;
    }

    public set isBold(value: boolean) {
        this._isBold = value;
    }

    public get isItalic(): boolean {
        return this._isItalic;
    }

    public set isItalic(value: boolean) {
        this._isItalic = value;
    }

    public get size(): any {
        return this._size;
    }

    public set size(value: any) {
        this._size = value;
    }

    public get color(): any {
        return this._color;
    }

    public set color(value: any) {
        this._color = value;
    }

    public get rotation(): any {
        return this._rotation;
    }

    public set rotation(value: any) {
        this._rotation = value;
    }
}

export class AxisDetails {
    private _title: any;
    private _axis: any;

    constructor($title: any, $axis: any) {
        const emptyTitleStyling = new AxisTitleStyling('', false, false, undefined, undefined);
        const emptyAxisStyling = new AxisStyling(true, false, false, undefined, undefined, undefined);
        this._title = $title ? $title : emptyTitleStyling;
        this._axis = $axis ? $axis : emptyAxisStyling;
    }

    public get title(): AxisTitleStyling {
        return this._title;
    }

    public set title(value: AxisTitleStyling) {
        this._title = value;
    }

    public get axis(): AxisStyling {
        return this._axis;
    }

    public set axis(value: AxisStyling) {
        this._axis = value;
    }
}

export class AxisConfig {
    public _horizontal: any;
    public _vertical: any;

    constructor($horizontal: any, $vertical: any) {
        const emptyTitleStyling = new AxisTitleStyling('', false, false, undefined, undefined);
        const emptyAxisStyling = new AxisStyling(true, false, false, undefined, undefined, undefined);
        const emptyAxisDetails =  new AxisDetails(emptyTitleStyling, emptyAxisStyling);
        this._horizontal = $horizontal ? $horizontal : emptyAxisDetails;
        this._vertical = $vertical ? $vertical : emptyAxisDetails;
    }

    public get horizontal(): AxisDetails {
        return this._horizontal;
    }

    public set horizontal(value: AxisDetails) {
        this._horizontal = value;
    }

    public get vertical(): AxisDetails {
        return this._vertical;
    }

    public set vertical(value: AxisDetails) {
        this._vertical = value;
    }
}