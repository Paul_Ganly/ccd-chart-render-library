export class SeriesConfig {
    private _display: boolean;
    private _size: number;
    private _color: string;

    constructor($display: boolean, $size: number, $color: string) {
        this._display = $display ? $display : true;
        this._size = $size ? $size : 12;
        this._color = $color ? $color : '#000';
    }

    public get display(): boolean {
        return this._display;
    }

    public set display(value: boolean) {
        this._display = value;
    }

    public get size(): number {
        return this._size;
    }

    public set size(value: number) {
        this._size = value;
    }

    public get color(): string {
        return this._color;
    }

    public set color(value: string) {
        this._color = value;
    }
}
