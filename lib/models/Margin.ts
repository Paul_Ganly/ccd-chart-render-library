export class Margin {
    private _top: number;
    private _right: number;
    private _bottom: number;
    private _left: number;

    constructor($top: number, $right: number, $bottom: number, $left: number) {
        this._top = $top ? $top : 0;
        this._right = $right ? $right : 0;
        this._bottom = $bottom ? $bottom : 0;
        this._left = $left ? $left : 0;
    }

    public get top(): number {
        return this._top;
    }

    public set top(value: number) {
        this._top = value;
    }

    public get right(): number {
        return this._right;
    }

    public set right(value: number) {
        this._right = value;
    }

    public get bottom(): number {
        return this._bottom;
    }

    public set bottom(value: number) {
        this._bottom = value;
    }

    public get left(): number {
        return this._left;
    }

    public set left(value: number) {
        this._left = value;
    }
}
