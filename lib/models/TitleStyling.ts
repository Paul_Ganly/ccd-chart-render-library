export class TitleStyling {
    private _titleText: string;
    private _isBold: boolean;
    private _isItalic: boolean;
    private _size: any;
    private _color: any;

    constructor($titleText: string, $isBold: boolean, $isItalic: boolean, $size: any, $color: any) {
        this._titleText = $titleText ? $titleText : '';
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
    }

    public get titleText(): string {
        return this._titleText;
    }

    public set titleText(value: string) {
        this._titleText = value;
    }

    public get isBold(): boolean {
        return this._isBold;
    }

    public set isBold(value: boolean) {
        this._isBold = value;
    }

    public get isItalic(): boolean {
        return this._isItalic;
    }

    public set isItalic(value: boolean) {
        this._isItalic = value;
    }

    public get size(): any {
        return this._size;
    }

    public set size(value: any) {
        this._size = value;
    }

    public get color(): any {
        return this._color;
    }

    public set color(value: any) {
        this._color = value;
    }
}
