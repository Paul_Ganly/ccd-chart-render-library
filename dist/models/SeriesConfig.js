"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SeriesConfig = /** @class */ (function () {
    function SeriesConfig($display, $size, $color) {
        this._display = $display ? $display : true;
        this._size = $size ? $size : 12;
        this._color = $color ? $color : '#000';
    }
    Object.defineProperty(SeriesConfig.prototype, "display", {
        get: function () {
            return this._display;
        },
        set: function (value) {
            this._display = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeriesConfig.prototype, "size", {
        get: function () {
            return this._size;
        },
        set: function (value) {
            this._size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeriesConfig.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    return SeriesConfig;
}());
exports.SeriesConfig = SeriesConfig;
