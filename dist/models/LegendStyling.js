"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LegendStyling = /** @class */ (function () {
    function LegendStyling($isBold, $isItalic, $size, $color) {
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
    }
    Object.defineProperty(LegendStyling.prototype, "isBold", {
        get: function () {
            return this._isBold;
        },
        set: function (value) {
            this._isBold = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LegendStyling.prototype, "isItalic", {
        get: function () {
            return this._isItalic;
        },
        set: function (value) {
            this._isItalic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LegendStyling.prototype, "size", {
        get: function () {
            return this._size;
        },
        set: function (value) {
            this._size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LegendStyling.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    return LegendStyling;
}());
exports.LegendStyling = LegendStyling;
