export declare class SeriesConfig {
    private _display;
    private _size;
    private _color;
    constructor($display: boolean, $size: number, $color: string);
    display: boolean;
    size: number;
    color: string;
}
