export declare class LegendSize {
    private _width;
    private _height;
    constructor($width: number, $height: number);
    width: number;
    height: number;
}
