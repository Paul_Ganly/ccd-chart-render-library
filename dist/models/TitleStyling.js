"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TitleStyling = /** @class */ (function () {
    function TitleStyling($titleText, $isBold, $isItalic, $size, $color) {
        this._titleText = $titleText ? $titleText : '';
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
    }
    Object.defineProperty(TitleStyling.prototype, "titleText", {
        get: function () {
            return this._titleText;
        },
        set: function (value) {
            this._titleText = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TitleStyling.prototype, "isBold", {
        get: function () {
            return this._isBold;
        },
        set: function (value) {
            this._isBold = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TitleStyling.prototype, "isItalic", {
        get: function () {
            return this._isItalic;
        },
        set: function (value) {
            this._isItalic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TitleStyling.prototype, "size", {
        get: function () {
            return this._size;
        },
        set: function (value) {
            this._size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TitleStyling.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    return TitleStyling;
}());
exports.TitleStyling = TitleStyling;
