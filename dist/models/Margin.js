"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Margin = /** @class */ (function () {
    function Margin($top, $right, $bottom, $left) {
        this._top = $top ? $top : 0;
        this._right = $right ? $right : 0;
        this._bottom = $bottom ? $bottom : 0;
        this._left = $left ? $left : 0;
    }
    Object.defineProperty(Margin.prototype, "top", {
        get: function () {
            return this._top;
        },
        set: function (value) {
            this._top = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Margin.prototype, "right", {
        get: function () {
            return this._right;
        },
        set: function (value) {
            this._right = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Margin.prototype, "bottom", {
        get: function () {
            return this._bottom;
        },
        set: function (value) {
            this._bottom = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Margin.prototype, "left", {
        get: function () {
            return this._left;
        },
        set: function (value) {
            this._left = value;
        },
        enumerable: true,
        configurable: true
    });
    return Margin;
}());
exports.Margin = Margin;
