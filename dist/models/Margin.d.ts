export declare class Margin {
    private _top;
    private _right;
    private _bottom;
    private _left;
    constructor($top: number, $right: number, $bottom: number, $left: number);
    top: number;
    right: number;
    bottom: number;
    left: number;
}
