export declare class AxisTitleStyling {
    private _titleText;
    private _isBold;
    private _isItalic;
    private _size;
    private _color;
    constructor($titleText: string, $isBold: boolean, $isItalic: boolean, $size: any, $color: any);
    titleText: string;
    isBold: boolean;
    isItalic: boolean;
    size: any;
    color: any;
}
export declare class AxisStyling {
    private _display;
    private _isBold;
    private _isItalic;
    private _size;
    private _color;
    private _rotation;
    constructor($display: boolean, $isBold: boolean, $isItalic: boolean, $size: any, $color: any, $rotation: any);
    display: boolean;
    isBold: boolean;
    isItalic: boolean;
    size: any;
    color: any;
    rotation: any;
}
export declare class AxisDetails {
    private _title;
    private _axis;
    constructor($title: any, $axis: any);
    title: AxisTitleStyling;
    axis: AxisStyling;
}
export declare class AxisConfig {
    _horizontal: any;
    _vertical: any;
    constructor($horizontal: any, $vertical: any);
    horizontal: AxisDetails;
    vertical: AxisDetails;
}
