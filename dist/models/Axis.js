"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AxisTitleStyling = /** @class */ (function () {
    function AxisTitleStyling($titleText, $isBold, $isItalic, $size, $color) {
        this._titleText = $titleText ? $titleText : '';
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
    }
    Object.defineProperty(AxisTitleStyling.prototype, "titleText", {
        get: function () {
            return this._titleText;
        },
        set: function (value) {
            this._titleText = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisTitleStyling.prototype, "isBold", {
        get: function () {
            return this._isBold;
        },
        set: function (value) {
            this._isBold = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisTitleStyling.prototype, "isItalic", {
        get: function () {
            return this._isItalic;
        },
        set: function (value) {
            this._isItalic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisTitleStyling.prototype, "size", {
        get: function () {
            return this._size;
        },
        set: function (value) {
            this._size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisTitleStyling.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    return AxisTitleStyling;
}());
exports.AxisTitleStyling = AxisTitleStyling;
var AxisStyling = /** @class */ (function () {
    function AxisStyling($display, $isBold, $isItalic, $size, $color, $rotation) {
        this._display = $display ? $display : true;
        this._isBold = $isBold ? $isBold : false;
        this._isItalic = $isItalic ? $isItalic : false;
        this._size = $size ? $size : { id: 1, value: 12 };
        this._color = $color ? $color : { id: 1, color: '#000' };
        this._rotation = $rotation ? $rotation : { id: 1, value: 0 };
    }
    Object.defineProperty(AxisStyling.prototype, "display", {
        get: function () {
            return this._display;
        },
        set: function (value) {
            this._display = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisStyling.prototype, "isBold", {
        get: function () {
            return this._isBold;
        },
        set: function (value) {
            this._isBold = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisStyling.prototype, "isItalic", {
        get: function () {
            return this._isItalic;
        },
        set: function (value) {
            this._isItalic = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisStyling.prototype, "size", {
        get: function () {
            return this._size;
        },
        set: function (value) {
            this._size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisStyling.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisStyling.prototype, "rotation", {
        get: function () {
            return this._rotation;
        },
        set: function (value) {
            this._rotation = value;
        },
        enumerable: true,
        configurable: true
    });
    return AxisStyling;
}());
exports.AxisStyling = AxisStyling;
var AxisDetails = /** @class */ (function () {
    function AxisDetails($title, $axis) {
        var emptyTitleStyling = new AxisTitleStyling('', false, false, undefined, undefined);
        var emptyAxisStyling = new AxisStyling(true, false, false, undefined, undefined, undefined);
        this._title = $title ? $title : emptyTitleStyling;
        this._axis = $axis ? $axis : emptyAxisStyling;
    }
    Object.defineProperty(AxisDetails.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (value) {
            this._title = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisDetails.prototype, "axis", {
        get: function () {
            return this._axis;
        },
        set: function (value) {
            this._axis = value;
        },
        enumerable: true,
        configurable: true
    });
    return AxisDetails;
}());
exports.AxisDetails = AxisDetails;
var AxisConfig = /** @class */ (function () {
    function AxisConfig($horizontal, $vertical) {
        var emptyTitleStyling = new AxisTitleStyling('', false, false, undefined, undefined);
        var emptyAxisStyling = new AxisStyling(true, false, false, undefined, undefined, undefined);
        var emptyAxisDetails = new AxisDetails(emptyTitleStyling, emptyAxisStyling);
        this._horizontal = $horizontal ? $horizontal : emptyAxisDetails;
        this._vertical = $vertical ? $vertical : emptyAxisDetails;
    }
    Object.defineProperty(AxisConfig.prototype, "horizontal", {
        get: function () {
            return this._horizontal;
        },
        set: function (value) {
            this._horizontal = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AxisConfig.prototype, "vertical", {
        get: function () {
            return this._vertical;
        },
        set: function (value) {
            this._vertical = value;
        },
        enumerable: true,
        configurable: true
    });
    return AxisConfig;
}());
exports.AxisConfig = AxisConfig;
