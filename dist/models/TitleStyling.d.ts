export declare class TitleStyling {
    private _titleText;
    private _isBold;
    private _isItalic;
    private _size;
    private _color;
    constructor($titleText: string, $isBold: boolean, $isItalic: boolean, $size: any, $color: any);
    titleText: string;
    isBold: boolean;
    isItalic: boolean;
    size: any;
    color: any;
}
