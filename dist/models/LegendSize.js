"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LegendSize = /** @class */ (function () {
    function LegendSize($width, $height) {
        this._width = $width ? $width : 0;
        this._height = $height ? $height : 0;
    }
    Object.defineProperty(LegendSize.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LegendSize.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    return LegendSize;
}());
exports.LegendSize = LegendSize;
