export declare class LegendStyling {
    private _isBold;
    private _isItalic;
    private _size;
    private _color;
    constructor($isBold: boolean, $isItalic: boolean, $size: any, $color: any);
    isBold: boolean;
    isItalic: boolean;
    size: any;
    color: any;
}
