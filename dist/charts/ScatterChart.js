"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var charts = require("./charts");
var ScatterChart = /** @class */ (function (_super) {
    __extends(ScatterChart, _super);
    function ScatterChart(isHeadlessRender, data, chart) {
        var _this = _super.call(this, chart) || this;
        _this.data = data;
        _this.isHeadlessRender = isHeadlessRender;
        return _this;
    }
    ScatterChart.prototype.renderObject = function (d3Lib) {
        var d3;
        if (this.isHeadlessRender) {
            d3 = d3Lib.d3;
        }
        else {
            d3 = d3Lib;
        }
        var width = this.width;
        var height = this.height;
        var padding = {
            top: 20,
            right: 20,
            bottom: 40,
            left: 50
        };
        // inner chart dimensions, where the dots are plotted
        var plotAreaWidth = width - padding.left - padding.right;
        var plotAreaHeight = height - padding.top - padding.bottom;
        // radius of points in the scatterplot
        var pointRadius = 3;
        var xScale = d3.scaleLinear().domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue]).range([0, plotAreaWidth]);
        var yScale = d3.scaleLinear().domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue]).range([plotAreaHeight, 0]);
        var rScale = d3.scaleLinear()
            .domain([0, this.scatterChartProperties.maxValue])
            .range([3, 3]);
        var color = d3.scaleLinear()
            .domain([this.scatterChartProperties.minValue, this.scatterChartProperties.maxValue])
            .range(['#eb001b', '#f79e1b'])
            .interpolate(d3.interpolateHcl);
        // init svg
        var svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        }
        else {
            svg = d3.select("svg#" + this.idx);
        }
        svg.attr('height', this.height)
            .attr('width', this.width)
            .append('g')
            .attr('transform', "translate(" + padding.left + ", " + padding.top + ")")
            .attr('class', 'group');
        // init axis
        var xAxisG = svg.append('g')
            .classed('x-axis', true)
            .attr('transform', "translate(0 " + (plotAreaHeight + pointRadius) + ")");
        var yAxisG = svg.append('g')
            .classed('y-axis', true)
            .attr('transform', "translate(" + -pointRadius + " 0)");
        // set up axis generating functions
        var xTicks = Math.round(plotAreaWidth / 50);
        var yTicks = Math.round(plotAreaHeight / 50);
        var xAxis = d3.axisBottom(xScale)
            .ticks(xTicks)
            .tickSizeOuter(0);
        var yAxis = d3
            .axisLeft(yScale)
            .ticks(yTicks)
            .tickSizeOuter(0);
        // draw axis
        yAxisG.call(yAxis);
        xAxisG.call(xAxis);
        var circles = svg.append('g')
            .attr('class', 'circles');
        // draw points
        circles.selectAll('.data-point')
            .data(this.data, function (d) {
            return d.id;
        })
            .enter()
            .append('circle')
            .classed('data-point', true)
            .attr('r', pointRadius)
            .attr('cx', function (d) { return xScale(d.x); })
            .attr('cy', function (d) { return yScale(d.y); })
            .attr('fill', function (d) { return color(d.y); })
            .attr('r', function (d) {
            return rScale(d.v);
        });
        return svg;
    };
    return ScatterChart;
}(charts.Chart));
exports.ScatterChart = ScatterChart;
