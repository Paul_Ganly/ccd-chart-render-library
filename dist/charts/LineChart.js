"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var charts = require("./charts");
var _ = require("lodash");
var LineChart = /** @class */ (function (_super) {
    __extends(LineChart, _super);
    function LineChart(isHeadlessRender, data, chart) {
        var _this = _super.call(this, chart) || this;
        _this.data = data;
        _this.isHeadlessRender = isHeadlessRender;
        return _this;
    }
    LineChart.prototype.renderObject = function (d3Lib) {
        var _this = this;
        var d3;
        if (this.isHeadlessRender) {
            d3 = d3Lib.d3;
        }
        else {
            d3 = d3Lib;
        }
        // spacing of title on axis x
        var fontTitleSpacingX = 0;
        if (this.axisConfig.horizontal.title.titleText.length) {
            fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
        }
        // spacing of title on axis y
        var fontTitleSpacingY = 0;
        if (this.axisConfig.vertical.title.titleText.length) {
            fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
        }
        // spacing of value labels
        var valuesLabelSpacing = 0;
        if (this.seriesConfig.display) {
            valuesLabelSpacing = this.seriesConfig.size;
        }
        var normalizedData = _.map(this.data.labels, function (item, index) {
            return {
                labels: _this.data.labels[index],
                values: _.map(_this.data.series, function (elem) {
                    return elem.values[index];
                })
            };
        });
        // init svg
        var svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        }
        else {
            svg = d3.select("svg#" + this.idx);
        }
        svg.attr('height', this.height)
            .attr('width', this.width);
        var XaxisLabels = _.map(this.data.series, function (series) { return series.label; });
        // Normalize array
        var valuesArray = [];
        for (var i = 0; i < this.data.labels.length; i++) {
            for (var j = 0; j < this.data.series.length; j++) {
                valuesArray.push(this.data.series[j].values[i]);
            }
        }
        svg.append('g')
            .attr('class', 'x-group')
            .selectAll('.labels-x')
            .data(XaxisLabels)
            .enter()
            .append('text')
            .text(function (d) { return d; })
            .style('font-size', this.axisConfig.horizontal.axis.size.value)
            .style('transform', function () {
            if (_this.axisConfig.horizontal.axis.rotation.value > 0) {
                return "rotate(-" + _this.axisConfig.horizontal.axis.rotation.value + "deg)";
            }
            else {
                return 'rotate(0deg)';
            }
        })
            .each(function () {
            _this.xAxisSpacing = d3.select('.x-group').node().getBBox().height;
        });
        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.x-group')).remove();
        }
        else {
            d3.select('.x-group').remove();
        }
        svg.append('g')
            .attr('class', 'y-group')
            .selectAll('.labels-y')
            .data(valuesArray)
            .enter()
            .append('text')
            .text(function (d) { return d; })
            .style('font-size', this.axisConfig.vertical.axis.size.value)
            .each(function () {
            _this.yAxisSpacing = d3.select('.y-group').node().getBBox().width;
        });
        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.y-group')).remove();
        }
        else {
            d3.select('.y-group').remove();
        }
        svg.append('g')
            .attr('class', 'last-x-label')
            .selectAll('.last-axis-label')
            .data([XaxisLabels.pop()])
            .enter()
            .append('text')
            .text(function (d) { return d; })
            .style('font-size', this.axisConfig.horizontal.axis.size.value)
            .style('transform', function () {
            if (_this.axisConfig.horizontal.axis.rotation.value > 0) {
                return "rotate(-" + _this.axisConfig.horizontal.axis.rotation.value + "deg)";
            }
            else {
                return 'rotate(0deg)';
            }
        })
            .each(function () {
            _this.lineChartProperties.lastXAxisWidth = d3.select('.last-x-label').node().getBBox().width;
        });
        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.last-x-label')).remove();
        }
        else {
            d3.select('.last-x-label').remove();
        }
        var width = this.width - this.yAxisSpacing - fontTitleSpacingY - this.margin.left;
        var height = this.height - this.xAxisSpacing - fontTitleSpacingX - this.margin.top - this.margin.bottom - valuesLabelSpacing;
        var g = svg.append('g')
            .attr('transform', "translate(" + this.margin.left + ", " + (this.margin.top + valuesLabelSpacing) + ")")
            .attr('class', 'group');
        // init axis
        var x = d3.scaleLinear().rangeRound([this.yAxisSpacing + fontTitleSpacingY, width + this.yAxisSpacing + fontTitleSpacingY - (this.lineChartProperties.lastXAxisWidth / 2) - (valuesLabelSpacing / 2)]);
        var y = d3.scaleLinear().rangeRound([height, 0]);
        x.domain([0, d3.max(normalizedData, function (d) {
                return d.values.length - 1;
            })]);
        y.domain([0, d3.max(valuesArray)]);
        // draw axis
        if (this.axisConfig.horizontal.axis.display) {
            // Axis configuration
            var xAxis = d3.axisBottom(x)
                .ticks(this.data.series.length - 1)
                .tickFormat(function (d, i) {
                return _this.data.series[i].label;
            });
            // Draw x axis
            g.append('g')
                .attr('class', 'axis x-axis')
                .attr('transform', 'translate(0,' + height + ')')
                .call(xAxis);
            // d3.selectAll('.x-axis line').style('display', 'none');
            // label styling
            var xAxisLabels = void 0;
            if (this.isHeadlessRender) {
                xAxisLabels = d3.select(d3Lib.document.querySelector('.x-axis text'));
            }
            else {
                xAxisLabels = d3.select('.x-axis text');
            }
            xAxisLabels.attr('class', function (d, i) {
                return "axis-label-" + i;
            })
                .attr('fill', this.axisConfig.horizontal.axis.color.color)
                .style('font-size', this.axisConfig.horizontal.axis.size.value)
                .style('font-weight', function () {
                return _this.axisConfig.horizontal.axis.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.horizontal.axis.isItalic ? 'italic' : 'normal';
            });
            if (this.axisConfig.horizontal.axis.display) {
                if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                    xAxisLabels.style('text-anchor', 'end')
                        .style('transform', "rotate(-" + this.axisConfig.horizontal.axis.rotation.value + "deg) translate(" + (-5 - (this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10) + "px, " + -(this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10 + "px)");
                }
            }
        }
        if (this.axisConfig.vertical.axis.display) {
            var yAxis = d3.axisLeft(y)
                .tickFormat(d3.format('d'));
            // Draw y axis
            g.append('g')
                .attr('class', 'axis y-axis')
                .attr('transform', "translate(" + (this.yAxisSpacing + fontTitleSpacingY) + ", 0)")
                .call(yAxis);
            // label styling
            var yAxisLabels = void 0;
            if (this.isHeadlessRender) {
                yAxisLabels = d3.select(d3Lib.document.querySelector('.y-axis text'));
            }
            else {
                yAxisLabels = d3.select('.y-axis text');
            }
            yAxisLabels.attr('class', function (d, i) {
                return "axis-label-" + i;
            })
                .attr('fill', this.axisConfig.vertical.axis.color.color)
                .style('font-size', this.axisConfig.vertical.axis.size.value)
                .style('font-weight', function () {
                return _this.axisConfig.vertical.axis.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.vertical.axis.isItalic ? 'italic' : 'normal';
            });
        }
        var lineHandler = d3.line()
            .x(function (d, i) { return x(i); })
            .y(function (d, i) { return y(d); });
        if (this.lineChartProperties.isCurved) {
            lineHandler = d3.line()
                .curve(d3.curveMonotoneX)
                .x(function (d, i) { return x(i); })
                .y(function (d, i) { return y(d); });
        }
        // angular 'this' issue
        var color = this.color;
        var lines = g.selectAll('.lines')
            .data(normalizedData)
            .enter()
            .append('g')
            .attr('class', 'lines');
        lines.append('path')
            .attr('class', 'line')
            .style('fill', 'none')
            .attr('d', function (d) { return lineHandler(d.values); })
            .style('stroke', function (d, i) { return color(d.labels); });
        lines.append('g')
            .attr('title', function (d) {
            return d.labels;
        })
            .selectAll('circle')
            .data(function (d) {
            return d.values;
        })
            .enter()
            .append('circle')
            .attr('r', 3)
            .attr('cx', function (d, i) { return x(i); })
            .attr('cy', function (d, i) { return y(d); })
            .attr('fill', 'white')
            .attr('stroke', function (d, i) {
            return color(d);
        });
        if (this.seriesConfig.display) {
            // labels
            lines
                .selectAll('text')
                .data(function (d) {
                return d.values;
            })
                .enter()
                .append('text')
                .attr('text-anchor', 'middle')
                .attr('x', function (d, i) { return x(i); })
                .attr('y', function (d, i) { return y(d) - 10; })
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .text(function (d, i) {
                return d;
            });
        }
        // x axis text
        if (this.axisConfig.horizontal.title.titleText.length > 0) {
            var xAxisText = void 0;
            if (this.isHeadlessRender) {
                xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
            }
            else {
                xAxisText = d3.select('.x-axis');
            }
            xAxisText.append('text')
                .attr('class', 'x-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .text(this.axisConfig.horizontal.title.titleText)
                .attr('fill', this.axisConfig.horizontal.title.color.color)
                .style('font-size', this.axisConfig.horizontal.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
            })
                .attr('x', function (d) {
                return _this.width / 2;
            })
                .attr('y', this.xAxisSpacing + 20);
        }
        // y axis text
        if (this.axisConfig.vertical.title.titleText.length > 0) {
            var yAxisText = void 0;
            if (this.isHeadlessRender) {
                yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
            }
            else {
                yAxisText = d3.select('.y-axis');
            }
            yAxisText.append('text')
                .attr('class', 'y-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .attr('x', function (d) {
                return -(height / 2);
            })
                .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
                .text(this.axisConfig.vertical.title.titleText)
                .attr('fill', this.axisConfig.vertical.title.color.color)
                .style('font-size', this.axisConfig.vertical.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
            })
                .style('transform', 'rotate(-90deg)');
        }
        return svg;
    };
    return LineChart;
}(charts.Chart));
exports.LineChart = LineChart;
