export declare class ScatterChartProperties {
    private _minValue;
    private _maxValue;
    constructor($minValue: number, $maxValue: number);
    minValue: number;
    maxValue: number;
}
