"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Chart = /** @class */ (function () {
    function Chart($chartProperties) {
        this._idx = $chartProperties.idx;
        this._chartType = $chartProperties.chartType;
        this._width = $chartProperties.width;
        this._height = $chartProperties.height;
        this._workspaceSize = $chartProperties.workspaceSize;
        this._titleHeight = $chartProperties.titleHeight;
        this._titleStyling = $chartProperties.titleStyling;
        this._legendSize = $chartProperties.legendSize;
        this._legendPosition = $chartProperties.legendPosition;
        this._legendStyling = $chartProperties.legendStyling;
        this._legendLabels = $chartProperties.legendLabels;
        this._seriesConfig = $chartProperties.seriesConfig;
        this._axisConfig = $chartProperties.axisConfig;
        this._xAxisSpacing = $chartProperties.xAxisSpacing;
        this._yAxisSpacing = $chartProperties.yAxisSpacing;
        this._color = $chartProperties.color;
        this._margin = $chartProperties.margin;
        this._colors = $chartProperties.colors;
        this._donutChartProperties = $chartProperties.donutChartProperties;
        this._lineChartProperties = $chartProperties.lineChartProperties;
        this._pieChartProperties = $chartProperties.pieChartProperties;
        this._scatterChartProperties = $chartProperties.scatterChartProperties;
    }
    Object.defineProperty(Chart.prototype, "idx", {
        get: function () {
            return this._idx;
        },
        set: function (value) {
            this._idx = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "chartType", {
        get: function () {
            return this._chartType;
        },
        set: function (value) {
            this._chartType = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "width", {
        get: function () {
            return this._width;
        },
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "height", {
        get: function () {
            return this._height;
        },
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "workspaceSize", {
        get: function () {
            return this._workspaceSize;
        },
        set: function (value) {
            this._workspaceSize = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "titleHeight", {
        get: function () {
            return this._titleHeight;
        },
        set: function (value) {
            this._titleHeight = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "titleStyling", {
        get: function () {
            return this._titleStyling;
        },
        set: function (value) {
            this._titleStyling = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "colors", {
        get: function () {
            return this._colors;
        },
        set: function (value) {
            this._colors = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "legendSize", {
        get: function () {
            return this._legendSize;
        },
        set: function (value) {
            this._legendSize = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "legendPosition", {
        get: function () {
            return this._legendPosition;
        },
        set: function (value) {
            this._legendPosition = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "legendStyling", {
        get: function () {
            return this._legendStyling;
        },
        set: function (value) {
            this._legendStyling = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "legendLabels", {
        get: function () {
            return this._legendLabels;
        },
        set: function (value) {
            this._legendLabels = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "seriesConfig", {
        get: function () {
            return this._seriesConfig;
        },
        set: function (value) {
            this._seriesConfig = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "axisConfig", {
        get: function () {
            return this._axisConfig;
        },
        set: function (value) {
            this._axisConfig = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "xAxisSpacing", {
        get: function () {
            return this._xAxisSpacing;
        },
        set: function (value) {
            this._xAxisSpacing = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "yAxisSpacing", {
        get: function () {
            return this._yAxisSpacing;
        },
        set: function (value) {
            this._yAxisSpacing = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "color", {
        get: function () {
            return this._color;
        },
        set: function (value) {
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "margin", {
        get: function () {
            return this._margin;
        },
        set: function (value) {
            this._margin = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "donutChartProperties", {
        get: function () {
            return this._donutChartProperties;
        },
        set: function (value) {
            this._donutChartProperties = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "lineChartProperties", {
        get: function () {
            return this._lineChartProperties;
        },
        set: function (value) {
            this._lineChartProperties = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "pieChartProperties", {
        get: function () {
            return this._pieChartProperties;
        },
        set: function (value) {
            this._pieChartProperties = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Chart.prototype, "scatterChartProperties", {
        get: function () {
            return this._scatterChartProperties;
        },
        set: function (value) {
            this._scatterChartProperties = value;
        },
        enumerable: true,
        configurable: true
    });
    return Chart;
}());
exports.Chart = Chart;
