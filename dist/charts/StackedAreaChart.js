"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var charts = require("./charts");
var StackedAreaChart = /** @class */ (function (_super) {
    __extends(StackedAreaChart, _super);
    function StackedAreaChart(isHeadlessRender, data, chart) {
        var _this = _super.call(this, chart) || this;
        _this.data = data;
        _this.isHeadlessRender = isHeadlessRender;
        return _this;
    }
    StackedAreaChart.prototype.renderObject = function (d3Lib) {
        var _this = this;
        var d3;
        if (this.isHeadlessRender) {
            d3 = d3Lib.d3;
        }
        else {
            d3 = d3Lib;
        }
        if (this.colors.length === 2) {
            this.color = d3
                .scaleLinear()
                .domain([0, this.data.length - 1])
                .interpolate(d3.interpolateRgb)
                .range(this.colors);
        }
        else {
            this.color = d3.scaleOrdinal().range(this.colors);
        }
        // spacing of title on axis x
        var fontTitleSpacingX = 0;
        if (this.axisConfig.horizontal.title.titleText.length) {
            fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
        }
        // spacing of title on axis y
        var fontTitleSpacingY = 0;
        if (this.axisConfig.vertical.title.titleText.length) {
            fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
        }
        var width = this.width - this.margin.left - this.margin.right;
        var height = this.height - this.margin.top - this.margin.bottom;
        var areaColors = this.color;
        var maxPointValueHeight = this.getMaxPointValue();
        var normalizedData = this.generateDataArrayForStack();
        // init axis
        var x = d3.scaleLinear().domain([0, d3.max(normalizedData, function (d) { return d.length - 1; })]).range([0, width]);
        var y = d3.scaleLinear().domain([0, maxPointValueHeight]).range([height, 0]);
        // Axis configuration
        var xAxis = d3.axisBottom(x)
            .scale(x)
            .ticks(this.data.series.length - 1)
            .tickFormat(function (d, i) {
            return _this.data.series[i].label;
        });
        var yAxis = d3.axisLeft(y).scale(y).tickFormat(d3.format('d'));
        var area = d3.area()
            .x(function (d) { return x(d.data.dataPoint); })
            .y0(function (d) { return y(d[0]); })
            .y1(function (d) { return y(d[1]); });
        var stack = d3.stack();
        // init svg
        var svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        }
        else {
            svg = d3.select("svg#" + this.idx);
        }
        svg.attr('height', this.height)
            .attr('width', this.width)
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .append('g')
            .attr('transform', "translate(" + this.margin.left + ", " + this.margin.top + ")")
            .attr('class', 'group');
        var keys = this.data.labels;
        // Set domains for axes
        x.domain(d3.extent(normalizedData, function (d) { return d.dataPoint; }));
        y.domain([0, maxPointValueHeight]);
        stack.keys(keys);
        stack.order(d3.stackOrderNone);
        stack.offset(d3.stackOffsetNone);
        var stackedAreas = svg.selectAll('.area')
            .data(stack(normalizedData))
            .enter().append('g')
            .attr('class', function (d) { return 'area ' + d.key; });
        stackedAreas.append('path')
            .attr('class', 'area')
            .attr('d', area)
            .style('fill', function (d) { return areaColors(d.key); });
        svg.append('g')
            .attr('class', 'axis x-axis')
            .attr('transform', "translate(0, " + height + ")")
            .call(xAxis);
        svg.append('g')
            .attr('class', 'y axis')
            .call(yAxis);
        // x axis text
        if (this.axisConfig.horizontal.title.titleText.length > 0) {
            var xAxisText = void 0;
            if (this.isHeadlessRender) {
                xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
            }
            else {
                xAxisText = d3.select('.x-axis');
            }
            xAxisText
                .append('text')
                .attr('class', 'x-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .text(this.axisConfig.horizontal.title.titleText)
                .attr('fill', this.axisConfig.horizontal.title.color.color)
                .style('font-size', this.axisConfig.horizontal.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
            })
                .attr('x', function (d) {
                return _this.width / 2;
            })
                .attr('y', this.xAxisSpacing + 20);
        }
        // y axis text
        if (this.axisConfig.vertical.title.titleText.length > 0) {
            var yAxisText = void 0;
            if (this.isHeadlessRender) {
                yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
            }
            else {
                yAxisText = d3.select('.y-axis');
            }
            yAxisText
                .append('text')
                .attr('class', 'y-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .attr('x', function (d) {
                return -(height / 2);
            })
                .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
                .text(this.axisConfig.vertical.title.titleText)
                .attr('fill', this.axisConfig.vertical.title.color.color)
                .style('font-size', this.axisConfig.vertical.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
            })
                .style('transform', 'rotate(-90deg)');
        }
        return svg;
    };
    /**
    * In order to use the d3 stack function we must arrange the data in a specific format.
    * @author paulganly
    * @date   2017-02-09
    */
    StackedAreaChart.prototype.generateDataArrayForStack = function () {
        var normalizedDataArray = [];
        var numberOfAreasToRender = this.data.labels.length;
        var numberDataPointsPerItem = this.data.series.length;
        for (var i = 0; i < numberDataPointsPerItem; i++) {
            var dataArrayRow = {};
            dataArrayRow['dataPoint'] = this.data.series[i].label;
            for (var j = 0; j < numberOfAreasToRender; j++) {
                dataArrayRow[this.data.labels[j]] = this.data.series[i].values[j];
            }
            normalizedDataArray.push(dataArrayRow);
        }
        return normalizedDataArray;
    };
    /**
     * We must calculate the max point height when all of the area are to be stacked on top of each-other so that
     * the y-axis is scaled correctly.
     * @author paulganly
     * @date   2017-02-09
     */
    StackedAreaChart.prototype.getMaxPointValue = function () {
        var seriesArrays = this.data.series;
        var numberOfPointValues = seriesArrays[0].values.length;
        var maxPointHeight = 0;
        for (var i = 0; i < numberOfPointValues; i++) {
            var currentPointHeight = 0;
            for (var j = 0; j < seriesArrays.length; j++) {
                currentPointHeight += seriesArrays[j].values[i];
            }
            if (currentPointHeight > maxPointHeight) {
                maxPointHeight = currentPointHeight;
            }
        }
        return maxPointHeight;
    };
    return StackedAreaChart;
}(charts.Chart));
exports.StackedAreaChart = StackedAreaChart;
