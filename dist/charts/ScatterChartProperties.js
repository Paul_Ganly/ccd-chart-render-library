"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ScatterChartProperties = /** @class */ (function () {
    function ScatterChartProperties($minValue, $maxValue) {
        this._minValue = $minValue ? $minValue : 0;
        this._maxValue = $maxValue ? $maxValue : 0;
    }
    Object.defineProperty(ScatterChartProperties.prototype, "minValue", {
        get: function () {
            return this._minValue;
        },
        set: function (value) {
            this._minValue = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ScatterChartProperties.prototype, "maxValue", {
        get: function () {
            return this._maxValue;
        },
        set: function (value) {
            this._maxValue = value;
        },
        enumerable: true,
        configurable: true
    });
    return ScatterChartProperties;
}());
exports.ScatterChartProperties = ScatterChartProperties;
