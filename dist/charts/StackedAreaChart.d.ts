import * as charts from './charts';
export declare class StackedAreaChart extends charts.Chart {
    private data;
    private isHeadlessRender;
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties);
    renderObject(d3Lib: any): any;
    /**
    * In order to use the d3 stack function we must arrange the data in a specific format.
    * @author paulganly
    * @date   2017-02-09
    */
    generateDataArrayForStack(): any[];
    /**
     * We must calculate the max point height when all of the area are to be stacked on top of each-other so that
     * the y-axis is scaled correctly.
     * @author paulganly
     * @date   2017-02-09
     */
    getMaxPointValue(): any;
}
