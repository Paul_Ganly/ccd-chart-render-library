"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var charts = require("./charts");
var _ = require("lodash");
var DonutChart = /** @class */ (function (_super) {
    __extends(DonutChart, _super);
    function DonutChart(isHeadlessRender, data, chart) {
        var _this = _super.call(this, chart) || this;
        _this.data = data;
        _this.isHeadlessRender = isHeadlessRender;
        return _this;
    }
    DonutChart.prototype.renderObject = function (d3Lib) {
        var _this = this;
        var d3;
        if (this.isHeadlessRender) {
            d3 = d3Lib.d3;
        }
        else {
            d3 = d3Lib;
        }
        var radius = Math.min(this.width, this.height) / 2;
        var outerRadius = radius - this.donutChartProperties.spaceForLabels;
        var innerRadius = Math.round(outerRadius / 2);
        // data totals
        var dataSum = d3.sum(this.data, function (d) { return d.value; });
        var chartData = _.map(this.data, function (item) { return item.value; });
        // section arc
        var sectionArc = d3.arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius)
            .padAngle(2 / 100)
            .cornerRadius(2);
        // shadow arc
        var shadowArc = d3.arc()
            .innerRadius(innerRadius - 5)
            .outerRadius(innerRadius + 10)
            .padAngle(2 / 100);
        // value calc
        var pieHandler = d3.pie()
            .sort(undefined)
            .value(function (d) {
            return d;
        });
        // init svg
        var svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        }
        else {
            svg = d3.select("svg#" + this.idx);
        }
        svg.attr('height', this.height)
            .attr('width', this.width)
            .append('g')
            .attr('transform', "translate(" + this.width / 2 + "," + radius + ")")
            .attr('class', 'group');
        // sections group
        var g = svg.selectAll('.arc')
            .data(pieHandler(chartData))
            .enter()
            .append('g')
            .attr('class', 'arc');
        // sections
        g.append('path')
            .attr('d', sectionArc)
            .attr('class', 'donut-slide')
            .attr('fill', function (d, i) {
            return _this.color(i);
        });
        if (this.donutChartProperties.shadows) {
            // section shadows group
            var shadow = svg.selectAll('.shadows')
                .data(pieHandler(chartData))
                .enter()
                .append('g')
                .attr('class', 'shadows');
            // section shadows
            shadow.append('path')
                .attr('d', shadowArc)
                .attr('fill', function (d, i) {
                var c = d3.hsl(_this.color(i));
                return d3.hsl((c.h + 5), (c.s - 0.07), (c.l - 0.15));
            });
        }
        if (this.seriesConfig.display) {
            // sections label
            svg.append('g')
                .attr('class', 'labels')
                .selectAll('text')
                .data(pieHandler(chartData))
                .enter()
                .append('text')
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .attr('transform', function (d) {
                return "translate(" + sectionArc.centroid(d) + ")";
            })
                .attr('dy', '.35em')
                .attr('text-anchor', function (d) {
                return (d.endAngle + d.startAngle) / 2 > Math.PI ? 'end' : 'start';
            })
                .attr('transform', function (d) {
                var labelr = outerRadius + 10;
                var c = sectionArc.centroid(d);
                var x = c[0];
                var y = c[1];
                // pythagorean theorem for hypotenuse
                var h = Math.sqrt((x * x) + (y * y));
                return "translate(" + ((x / h) * labelr) + "," + ((y / h) * labelr) + ")";
            })
                .text(function (d) {
                var value = (d.value / dataSum) * 100;
                return d3.format('.3g')(value) + "%";
            });
        }
        return svg;
    };
    return DonutChart;
}(charts.Chart));
exports.DonutChart = DonutChart;
