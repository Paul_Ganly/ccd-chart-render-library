import * as charts from './charts';
export declare class StackedBarChartVertical extends charts.Chart {
    private data;
    private isHeadlessRender;
    constructor(isHeadlessRender: boolean, data: any, chart: charts.IChartProperties);
    renderObject(d3Lib: any): any;
}
