"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DonutChartProperties = /** @class */ (function () {
    function DonutChartProperties($shadows, $spaceForLabels) {
        this._shadows = $shadows ? $shadows : false;
        this._spaceForLabels = $spaceForLabels ? $spaceForLabels : 0;
    }
    Object.defineProperty(DonutChartProperties.prototype, "shadows", {
        get: function () {
            return this._shadows;
        },
        set: function (value) {
            this._shadows = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DonutChartProperties.prototype, "spaceForLabels", {
        get: function () {
            return this._spaceForLabels;
        },
        set: function (value) {
            this._spaceForLabels = value;
        },
        enumerable: true,
        configurable: true
    });
    return DonutChartProperties;
}());
exports.DonutChartProperties = DonutChartProperties;
