"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LineChartProperties = /** @class */ (function () {
    function LineChartProperties($isCurved, $lastXAxisWidth) {
        this._isCurved = $isCurved ? $isCurved : false;
        this._lastXAxisWidth = $lastXAxisWidth ? $lastXAxisWidth : 0;
    }
    Object.defineProperty(LineChartProperties.prototype, "isCurved", {
        get: function () {
            return this._isCurved;
        },
        set: function (value) {
            this._isCurved = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LineChartProperties.prototype, "lastXAxisWidth", {
        get: function () {
            return this._lastXAxisWidth;
        },
        set: function (value) {
            this._lastXAxisWidth = value;
        },
        enumerable: true,
        configurable: true
    });
    return LineChartProperties;
}());
exports.LineChartProperties = LineChartProperties;
