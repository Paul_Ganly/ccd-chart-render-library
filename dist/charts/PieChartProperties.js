"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PieChartProperties = /** @class */ (function () {
    function PieChartProperties($spaceForLabels) {
        this._spaceForLabels = $spaceForLabels ? $spaceForLabels : 0;
    }
    Object.defineProperty(PieChartProperties.prototype, "spaceForLabels", {
        get: function () {
            return this._spaceForLabels;
        },
        set: function (value) {
            this._spaceForLabels = value;
        },
        enumerable: true,
        configurable: true
    });
    return PieChartProperties;
}());
exports.PieChartProperties = PieChartProperties;
