export declare class DonutChartProperties {
    private _shadows;
    private _spaceForLabels;
    constructor($shadows: boolean, $spaceForLabels: number);
    shadows: boolean;
    spaceForLabels: number;
}
