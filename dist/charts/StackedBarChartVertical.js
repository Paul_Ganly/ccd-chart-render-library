"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var charts = require("./charts");
var _ = require("lodash");
var StackedBarChartVertical = /** @class */ (function (_super) {
    __extends(StackedBarChartVertical, _super);
    function StackedBarChartVertical(isHeadlessRender, data, chart) {
        var _this = _super.call(this, chart) || this;
        _this.data = data;
        _this.isHeadlessRender = isHeadlessRender;
        return _this;
    }
    StackedBarChartVertical.prototype.renderObject = function (d3Lib) {
        var _this = this;
        var d3;
        if (this.isHeadlessRender) {
            d3 = d3Lib.d3;
        }
        else {
            d3 = d3Lib;
        }
        if (this.colors.length === 2) {
            this.color = d3
                .scaleLinear()
                .domain([0, this.data.length - 1])
                .interpolate(d3.interpolateRgb)
                .range(this.colors);
        }
        else {
            this.color = d3.scaleOrdinal().range(this.colors);
        }
        // spacing of title on axis x
        var fontTitleSpacingX = 0;
        if (this.axisConfig.horizontal.title.titleText.length) {
            fontTitleSpacingX = this.axisConfig.horizontal.title.size.value;
        }
        // spacing of title on axis y
        var fontTitleSpacingY = 0;
        if (this.axisConfig.vertical.title.titleText.length) {
            fontTitleSpacingY = this.axisConfig.vertical.title.size.value;
        }
        // Total values
        var totalValues = [];
        var sum;
        for (var i = 0; i < this.data.series[0].values.length; i++) {
            sum = 0;
            for (var j = 0; j < this.data.series.length; j++) {
                sum += this.data.series[j].values[i];
            }
            totalValues.push(sum);
        }
        // Normalized array
        var normalizedData = [];
        for (var i = 0; i < this.data.series[0].values.length; i++) {
            for (var j = 0; j < this.data.series.length; j++) {
                normalizedData.push(this.data.series[j].values[i]);
            }
        }
        // init svg
        var svg;
        if (this.isHeadlessRender) {
            svg = d3.select(d3Lib.document.querySelector('svg'));
        }
        else {
            svg = d3.select("svg#" + this.idx);
        }
        svg.attr('height', this.height)
            .attr('width', this.width);
        svg.append('g')
            .attr('class', 'x-group')
            .selectAll('.labels-x')
            .data(this.data.labels)
            .enter()
            .append('text')
            .text(function (d) { return d; })
            .style('font-size', this.axisConfig.horizontal.axis.size.value)
            .style('transform', function () {
            if (_this.axisConfig.horizontal.axis.rotation.value > 0) {
                return "rotate(-" + _this.axisConfig.horizontal.axis.rotation.value + "deg)";
            }
            else {
                return 'rotate(0deg)';
            }
        })
            .each(function () {
            if (_this.isHeadlessRender) {
                if (d3.select(d3Lib.document.querySelector('.x-group')).node()) {
                    _this.xAxisSpacing = d3.select(d3Lib.document.querySelector('.x-group')).node().getBBox().height;
                }
            }
            else {
                if (d3.select('.x-group').node()) {
                    _this.xAxisSpacing = d3.select('.x-group').node().getBBox().height;
                }
            }
        });
        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.x-group')).remove();
        }
        else {
            d3.select('.x-group').remove();
        }
        svg.append('g')
            .attr('class', 'y-group')
            .selectAll('.labels-y')
            .data(normalizedData)
            .enter()
            .append('text')
            .text(function (d) { return d; })
            .style('font-size', this.axisConfig.vertical.axis.size.value)
            .each(function () {
            if (_this.isHeadlessRender) {
                if (d3.select(d3Lib.document.querySelector('.y-group')).node()) {
                    _this.yAxisSpacing = d3.select(d3Lib.document.querySelector('.y-group')).node().getBBox().width;
                }
            }
            else {
                if (d3.select('.y-group').node()) {
                    _this.yAxisSpacing = d3.select('.y-group').node().getBBox().width;
                }
            }
        });
        if (this.isHeadlessRender) {
            d3.select(d3Lib.document.querySelector('.y-group')).remove();
        }
        else {
            d3.select('.y-group').remove();
        }
        var empty = '';
        var height = this.height - this.xAxisSpacing - fontTitleSpacingX - this.margin.top - this.margin.bottom - 5; // hardcoded: 5 is added to not cut horizontal title
        var width = this.width - this.yAxisSpacing - fontTitleSpacingY - this.margin.left;
        var gapBetweenGroups = (this.width / this.data.labels.length) / 5;
        var barWidth = ((width - (gapBetweenGroups * this.data.labels.length)) / this.data.labels.length);
        // this.spaceForAxis = 45 * 2;
        // Labels array
        var aLabels = _.map(this.data.series, function (element, index) {
            return _this.data.series[index].label;
        });
        // Calculate width based on data length
        var chartWidth = (barWidth + gapBetweenGroups) * this.data.labels.length;
        // Set group for axis labels
        var g = svg.append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
        // init axis
        var x = d3.scaleBand().rangeRound([this.yAxisSpacing + fontTitleSpacingY, width + this.yAxisSpacing + fontTitleSpacingY]);
        var y = d3.scaleLinear().rangeRound([height, 0]);
        x.domain(_.map(this.data.labels, function (d) { return d; }));
        y.domain([0, d3.max(totalValues)]);
        // draw axis
        if (this.axisConfig.horizontal.axis.display) {
            // Axis configuration
            var xAxis = g.append('g')
                .attr('class', 'axis x-axis')
                .attr('transform', "translate(0, " + height + ")")
                .call(d3.axisBottom(x));
            if (this.isHeadlessRender) {
                d3.select(d3Lib.document.querySelector('.x-axis line')).style('display', 'none');
            }
            else {
                d3.select('.x-axis line').style('display', 'none');
            }
            var xAxisLabel = void 0;
            if (this.isHeadlessRender) {
                xAxisLabel = d3.select(d3Lib.document.querySelector('.x-axis text'));
            }
            else {
                xAxisLabel = d3.select('.x-axis text');
            }
            // label styling
            xAxisLabel
                .attr('class', function (d, i) {
                return "axis-label-" + i;
            })
                .attr('fill', this.axisConfig.horizontal.axis.color.color)
                .style('font-size', this.axisConfig.horizontal.axis.size.value)
                .style('font-weight', function () {
                return _this.axisConfig.horizontal.axis.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.horizontal.axis.isItalic ? 'italic' : 'normal';
            });
            if (this.axisConfig.horizontal.axis.display) {
                if (this.axisConfig.horizontal.axis.rotation.value > 0) {
                    xAxisLabel.style('text-anchor', 'end')
                        .style('transform', "rotate(-" + this.axisConfig.horizontal.axis.rotation.value + "deg) translate(" + (-5 - (this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10) + "px, " + -(this.axisConfig.horizontal.axis.rotation.value / 30 - 1) * 10 + "px)");
                }
            }
        }
        if (this.axisConfig.vertical.axis.display) {
            var yAxis = d3.axisLeft(y)
                .ticks(10)
                .tickFormat(function (d) {
                return parseInt(d, 10);
            }).tickSizeInner((chartWidth * -1));
            // Draw y axis
            g.append('g')
                .attr('class', 'axis y-axis')
                .attr('transform', "translate(" + (this.yAxisSpacing + fontTitleSpacingY) + ", 0)")
                .call(yAxis);
            // label styling
            var yAxisLabel = void 0;
            if (this.isHeadlessRender) {
                yAxisLabel = d3.select(d3Lib.document.querySelector('.y-axis text'));
            }
            else {
                yAxisLabel = d3.select('.y-axis text');
            }
            yAxisLabel.attr('class', function (d, i) {
                return "axis-label-" + i;
            })
                .attr('fill', this.axisConfig.vertical.axis.color.color)
                .style('font-size', this.axisConfig.vertical.axis.size.value)
                .style('font-weight', function () {
                return _this.axisConfig.vertical.axis.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.vertical.axis.isItalic ? 'italic' : 'normal';
            });
        }
        // Set group for all bars
        var barsGroup = svg.append('g')
            .attr('transform', 'translate(' + (this.margin.left + this.yAxisSpacing + fontTitleSpacingY) + ',' + 0 + ')');
        // Create bars
        var bar = barsGroup.selectAll('.groups')
            .data(normalizedData)
            .enter()
            .append('g')
            .attr('class', 'groups')
            .attr('transform', function (d, i) {
            return "translate(" + ((((Math.floor(i / _this.data.series.length)) * barWidth) + gapBetweenGroups * (0.5 + Math.floor(i / _this.data.series.length)))) + ", " + (y(d) + _this.margin.top) + ")";
        });
        // Create rectangles of the correct height
        var barPositionY = 0;
        bar.append('rect')
            .attr('fill', function (d, i) {
            return _this.color(i % _this.data.series.length);
        })
            .attr('class', 'bar')
            .attr('width', barWidth)
            .attr('height', function (d) {
            return height - y(d);
        })
            .attr('y', function (d, i) {
            if ((i % _this.data.series.length) === 0) {
                barPositionY = 0;
                return barPositionY;
            }
            for (var j = i; j < normalizedData.length; j++) {
                if ((i - j) % _this.data.series.length === 0) {
                    barPositionY -= (height - y(normalizedData[i - 1]));
                    return barPositionY;
                }
            }
        });
        // Draw labels
        if (this.seriesConfig.display) {
            var textPositionY_1 = 0;
            var stackedPosition_1 = 0;
            bar.append('text')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'middle')
                .attr('y', function (d, i) {
                if ((i % _this.data.series.length) === 0) {
                    textPositionY_1 = height - y(normalizedData[i]);
                    stackedPosition_1 = height - y(normalizedData[i]);
                    return textPositionY_1 / 2;
                }
                for (var j = i; j < normalizedData.length; j++) {
                    textPositionY_1 = height - y(normalizedData[j]);
                    stackedPosition_1 += height - y(normalizedData[j]);
                    return (textPositionY_1 / 2) - stackedPosition_1 + (height - y(normalizedData[j]));
                }
            })
                .attr('x', function (d, i) {
                return barWidth / 2;
            })
                .attr('fill', this.seriesConfig.color)
                .style('font-size', this.seriesConfig.size)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .text(function (d) { return d; });
        }
        // x axis text
        if (this.axisConfig.horizontal.title.titleText.length > 0) {
            var xAxisText = void 0;
            if (this.isHeadlessRender) {
                xAxisText = d3.select(d3Lib.document.querySelector('.x-axis'));
            }
            else {
                xAxisText = d3.select('.x-axis');
            }
            xAxisText
                .append('text')
                .attr('class', 'x-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .text(this.axisConfig.horizontal.title.titleText)
                .attr('fill', this.axisConfig.horizontal.title.color.color)
                .style('font-size', this.axisConfig.horizontal.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.horizontal.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.horizontal.title.isItalic ? 'italic' : 'normal';
            })
                .attr('x', function (d) {
                return _this.width / 2;
            })
                .attr('y', this.xAxisSpacing + 20);
        }
        // y axis text
        if (this.axisConfig.vertical.title.titleText.length > 0) {
            var yAxisText = void 0;
            if (this.isHeadlessRender) {
                yAxisText = d3.select(d3Lib.document.querySelector('.y-axis'));
            }
            else {
                yAxisText = d3.select('.y-axis');
            }
            yAxisText
                .append('text')
                .attr('class', 'y-title')
                .attr('text-anchor', 'middle')
                .attr('alignment-baseline', 'hanging')
                .attr('x', function (d) {
                return -(height / 2);
            })
                .attr('y', -this.yAxisSpacing - fontTitleSpacingY - 20)
                .text(this.axisConfig.vertical.title.titleText)
                .attr('fill', this.axisConfig.vertical.title.color.color)
                .style('font-size', this.axisConfig.vertical.title.size.value)
                .style('font-family', 'mark-mc-narrow, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif')
                .style('font-weight', function () {
                return _this.axisConfig.vertical.title.isBold ? 'bold' : 'normal';
            })
                .style('font-style', function () {
                return _this.axisConfig.vertical.title.isItalic ? 'italic' : 'normal';
            })
                .style('transform', 'rotate(-90deg)');
        }
        return svg;
    };
    return StackedBarChartVertical;
}(charts.Chart));
exports.StackedBarChartVertical = StackedBarChartVertical;
