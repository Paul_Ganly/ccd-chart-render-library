export declare class LineChartProperties {
    private _isCurved;
    private _lastXAxisWidth;
    constructor($isCurved: boolean, $lastXAxisWidth: number);
    isCurved: boolean;
    lastXAxisWidth: number;
}
